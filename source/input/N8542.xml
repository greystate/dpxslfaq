<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-05-08 12:21:54 dpawson"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="N8542">
<head>
<title>Columns</title>
<summary>Columns</summary>
      <keywords>xsl, xslt, xsl-fo, columns</keywords>
      <description>columns in xsl</description>
</head>
<qandaset>
<qandaentry>

        <question>
<para>Half page with multicolumn and the other half page without columns</para>
</question>
    
        

        

            

            

        

        <answer>
<para role="author">David Tolpin</para>

            <para>No, currently it is not possible, because column-count
is a property of fo:region-body, and there can be exactly one
fo:region-body on a page.  However, &lt;fo:block span=all&gt; may be used
as a hack, but it is very difficult to position such blocks properly.

            </para>

        </answer>

    </qandaentry>
<qandaentry>
   <question>
    <para>newspaper layout with FOs</para>

   </question>
   <answer>
    <para role="author">Arved Sandstrom</para>
    <para>
XSL 1.0 is heavily attuned to content-driven layout: one central body region 
and up to 4 peripheral regions per page master, page masters generally 
intended to select on rather unsophisticated conditions (first or last page, 
even/odd, etc), and most devastating for what you want: exactly _one_ flow 
per page-sequence.
	</para>
<para>You pretty much have to wait for XSL 2.0 to see layout-driven support. You 
can find papers dating back to '98, say (check out 
http://www.seyboldseminars.com/Events/sf98/transcripts/ETAPE_30.html, 
Stephen Deach's part near the end, for example), when the first XSL draft 
just came out, where the WG people are talking about the flow-map object 
that is only mentioned as an aside in the CR, and at that time they were 
also talking about multiple "content queues" per page sequence, switched 
into various regions by the flow-map. This all went away when reality set 
in. :-)</para>
<para>
(For those who are curious about the difference, Deach has a useful paper, 
one URL for which is http://www.infoloom.com/gcaconfs/WEB/paris98/deach.HTM).</para>
<para>
Ironically the issue may have been more with the spec...who was happy with 
what feature, in other words. Because N flows are about as simple to 
implement as one, and if you can properly handle 1-5 regions on each of a 
whole bunch of page-masters, you sure as heck can handle more than 5.
</para>
   </answer>
  </qandaentry>



    <qandaentry>
      <question>
	<para>Spanning text across a two column page</para>
      </question>
      <answer>
	<para role="author">Ken Holman</para>
	<literallayout>

>How can I span some text into 2 columns in a page context with a
>2-column layout?
	</literallayout>
<para>By using the span="all" property on the blocks that you want to span on the 
page.</para>
<para>
Note in XSL-FO 1.0 you can only span all of the columns or keep the width 
to the one column you are in.</para>
      </answer>
    </qandaentry>
    <qandaentry>
      <question>
	<para>Multi-column text</para>
      </question>
      <answer>
	<para role="author">Oleg Tkachenko</para>
	<literallayout>


> I wanted to see if anyone has done something similar to this and 
> offer implementation suggestions. 
> In our DTD, we have a list type that allows the user to specify 
> the number of columns that they would like the list to format in 
> via an attribute on the list element. 
> The list then contains a series of 2 or more items that are just PCDATA.
> 
> Basically, I need to count the number of items in the list and
> divide it by the number of columns they specified. 
> The items should then fill each column from top to bottom and then 
> any remainders would be placed in the last column.
> 
> For example if the list contained 5 items and the user specified 
> they wanted 3 columns it would look like so:
> 
> item 1	item 3	item 5    
> item 2	item 4
> 
> Or if the list contained 5 items and the user specified 
> they wanted 2 columns it would look like so:
> 
> item 1	item 4
> item 2	item 5
> item 3
	</literallayout>
<para>Have you considered native declarative xsl-fo solution? Just declare how much 
columns you want on a region-body and let your favorite formatter to format 
your items appropriatively.</para>
	<programlisting>
&lt;fo:region-body column-count="{$user-defined-number-of-columns}" 
column-gap="10pt"/>
</programlisting>
	<para>See
1. spec:
	  <ulink url="http://www.w3.org/TR/xsl/slice6.html#fo_region-body">xsl rec</ulink>
2. example:
	  "Multi-Column Regions" example at <ulink url="http://www.renderx.com/testcases.html">RenderX examples</ulink>.
</para>      </answer>
    </qandaentry>

    <qandaentry>
      <question>
	<para>Two columns aligned at the end</para>
      </question>
      <answer>
	<para role="author">Ken Holman</para>
	<literallayout>

>I want a 2 column composition to be bottom aligned.
>I mean that instead of being aligned on the top of each column, the last
>baselines should be aligned, not the top line.
</literallayout>
<para>
Use display-align="after" on the &lt;region-body> and all columns will be 
aligned to the after edge of the page ... which doesn't mean last baselines 
if you are using different font sizes, but if you are using the same font 
size then the baselines will coincidentally aligned on the last line.</para>


      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>One and two columns in single page?</para>
      </question>
      <answer>
	<para role="author">W. Eliot Kimber</para>
	<literallayout>
> I've been trying to render a physical
> page in which the top half of the page has a single column
> of text (centered), and the bottom half has a dual column
> format.  The dual column text should then flow onto other
> body pages formatted entirely for dual column, which I handle
> with different page-masters.
</literallayout>
<para>To do this you have to define your fo:region-body to have two columns 
and then use span="all" on the blocks that should *not* be two columns.</para>

<para>Note that span= can only legally be specified on blocks that are direct 
children of fo:flow, which can complicate your block generation a little 
bit if you're in the habit of just blindly throwing container blocks 
around stuff whether they are really needed or not. Not all FO 
implementations enforce this rule, but some do.</para>

<para>To balance columns you follow a multi-column block by an empty, 
zero-height spanning block.</para>

      </answer>
    </qandaentry>

    <qandaentry>
      <question>
	<para>How to balance columns</para>
      </question>
      <answer>
	<para role="author">W. Eliot Kimber</para>
	<literallayout>
> I have a two column book. I place a page break to start a new section
> and the page before the break does not balance the columns.
> 
> Is this possible and how? I'm using Antenna House.
</literallayout>
<para>
To balance columns issue an empty block with span="all":</para>

<programlisting>&lt;fo:block>Content in multiple columns&lt;/fo:block>
&lt;fo:block span="all">&lt;fo:leader leader-length="0pt" 
leader-pattern="space"/>&lt;/fo:block></programlisting>

<para>
The empty leader is to ensure that the block is honored as some 
processors will discard empty blocks.</para>
<para>
This should work in all FO implementations that support multiple columns.</para>



      </answer>
    </qandaentry>
 <qandaentry>
      <question>
	<para>Balanced two column ending</para>
      </question>
      <answer>
	<para role="author">Colin Shapiro | Ken Holman</para>



<para>"Yes, include an empty spanned block at the end of your flow. The formatter
will try to balance all of your columns on top of this empty block. Modulo
keeps, breaks, widows, orphans, etc."</para>

<para>With XEP, you can omit the empty spanned block and just use a flow-section
(one of the RenderX extensions).  The columns inside of your flow-section
	  will be balanced. See <ulink url="http://www.renderx.net/lists/xep-support/1119.html">xep lists</ulink></para>



      </answer>
    </qandaentry>

</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
