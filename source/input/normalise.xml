<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2002-09-28 11:11:11 Dave Pawson"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="normalise">
<config param="desc" value="nomalise"/>
<config param="dir" value="xsl"/>
<config param="filename" value="normalise.html"/>
<head>
<title>Normalise-space</title>
<summary>Removing White space</summary>
      <keywords>xslt, whitespace</keywords>
      <description>xslt and whitespace, normalize-space</description>
</head>
<qandaset>




  <qandaentry>
   <question>
    <para>normalize-space, what does it do?</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">

> Using normalize-space is the only way I've found to eliminate the
> newlines, but then I lose the whitespace around the command elements
> is stripped.</literallayout>
<para>
That's because normalize-space() does three things:</para>
	<literallayout>
  - replaces whitespace characters with spaces
  - strips leading and trailing whitespace
  - collapses sequences of whitespace into single spaces
</literallayout>
<para>It's the second of these that's causing you to lose whitespace. You
only want to do the first to get rid of the newline characters, so
instead of using normalize-space(), use the translate() function to
replace any &amp;#xA; or &amp;#xD; characters with spaces:</para>
	<programlisting>
&lt;xsl:template match="text()">
  &lt;xsl:value-of select="translate(., '&amp;#xA;&amp;#xD;', '  ')" />
&lt;/xsl:template>
</programlisting>  
	<literallayout>> The preserve-space (and strip-space) functions don't seem to have
> any effect, presumably because refpurpose is not a text-only
> element.
</literallayout>
	<para>The xsl:preserve-space and xsl:strip-space elements govern how
whitespace-only text nodes are handled -- whether they're ignored or
preserved. In your document, the text nodes you're worried about have
other characters in them as well as whitespace, so they won't be
affected (although you'd be well advised not to use xsl:strip-space as
if you ever did have a whitespace-only text node within your
refpurpose, the likelihood is that you'd want to keep it).
</para>

   </answer>
  </qandaentry>



<qandaentry>
 <question>
   <para>How to normalise strings containing subelement</para>
 </question>
 
<answer>
    <para role="author">David Carlisle</para>
    <para>

     <emphasis>Note:</emphasis>Entities are no problem as they are all expanded before XSL sees the
document.
    </para>
    <para>Normalising space in mixed content is _always_ a problem.
Of course normally you just don't do it, as the renderer (eg html in
this case) can more easily do it as it lays out the characters, after
all the markup is gone.</para>

    <para>If you do want to do it within the markup, the problem as stated was
underspecified, but here is a possible (probably partial) solution.
As always documented to the "literate programming" standards discussed
in an earlier thread on this list.</para>



    <programlisting>&lt;x>
&lt;para>Some    text    &lt;em>some    other   text&lt;/em>   remaining text&lt;/para>
&lt;para>Some    text&lt;em>    some    other   text&lt;/em>   remaining text&lt;/para>
&lt;para>Some    text    &lt;em>   some    other   text&lt;/em>   remaining
text&lt;/para>
&lt;para>Some    text    &lt;em>some    other   text &lt;/em>   remaining text&lt;/para>
&lt;para>Some    text    &lt;em>some    other   text &lt;/em>remaining text&lt;/para>
&lt;para> Some    text    &lt;em>some    other   text&lt;/em>   remaining text
&lt;/para>
&lt;/x>
</programlisting>




    <programlisting>&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                >

&lt;xsl:output method="xml" indent="yes"/>

&lt;xsl:template match="*">
&lt;xsl:copy>
&lt;xsl:copy-of select="@*"/>
&lt;xsl:apply-templates/>
&lt;/xsl:copy>
&lt;/xsl:template>


&lt;xsl:template match="para/text()">
&lt;xsl:value-of select="normalize-space(.)"/>
&lt;xsl:if test="contains(concat(.,'^$%'),' ^$%') and following-sibling::* and
 not(following-sibling::*[1]/node()[1][self::text() and starts-with(.,'
')])">
  &lt;xsl:text> &lt;/xsl:text>
&lt;/xsl:if>
&lt;/xsl:template>



&lt;xsl:template match="para/*/text()">
&lt;xsl:if test="starts-with(.,' ')">&lt;xsl:text> &lt;/xsl:text>&lt;/xsl:if>
&lt;xsl:value-of select="normalize-space(.)"/>
&lt;xsl:if test="contains(concat(.,'^$%'),' ^$%') or 
  ../following-sibling::node()[1][self::text() and starts-with(.,' ')]">
  &lt;xsl:text> &lt;/xsl:text>
&lt;/xsl:if>
&lt;/xsl:template>

&lt;/xsl:stylesheet>
</programlisting>

    <programlisting>bash-2.01$ xt normsp.xml normsp.xsl 
&lt;?xml version="1.0" encoding="utf-8"?>
&lt;x>
&lt;para>Some text &lt;em>some other text &lt;/em>remaining text&lt;/para>
&lt;para>Some text&lt;em> some other text &lt;/em>remaining text&lt;/para>
&lt;para>Some text&lt;em> some other text &lt;/em>remaining text&lt;/para>
&lt;para>Some text &lt;em>some other text &lt;/em>remaining text&lt;/para>
&lt;para>Some text &lt;em>some other text &lt;/em>remaining text&lt;/para>
&lt;para>Some text &lt;em>some other text &lt;/em>remaining text&lt;/para>
&lt;/x>

</programlisting>


   </answer>
  </qandaentry>


  <qandaentry>
   <question>
    <para>Normalising space in mixed content.</para>

   </question>
   <answer>
    <para role="author">David Carlisle and Wendell Piez</para>
    <para>

Why not strip it all, then put it back where you want it? So....</para>

    <programlisting>&lt;xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

&lt;xsl:output method="xml" indent="yes"/>

&lt;xsl:template match="text()">
  &lt;!-- strip extra whitespace from text nodes
       (including leading and trailing whitespace) -->
  &lt;xsl:value-of select="normalize-space(.)"/>
&lt;/xsl:template>

&lt;xsl:template match="*">
  &lt;!-- default element rule is identity transform -->
  &lt;xsl:copy>
    &lt;xsl:copy-of select="@*"/>
    &lt;xsl:apply-templates/>
  &lt;/xsl:copy>
&lt;/xsl:template>

&lt;xsl:template match="*[../text()[normalize-space(.) != '']]">
  &lt;!-- but this template matches any element appearing in mixed content -->
  &lt;xsl:variable name="textbefore"
       select="preceding-sibling::node()[1][self::text()]"/>
  &lt;xsl:variable name="textafter"
       select="following-sibling::node()[1][self::text()]"/>
  &lt;!-- Either of the preceding variables will be an empty node set 
       if the neighbor node is not text(), right? -->
  &lt;xsl:variable name="prevchar"
       select="substring($textbefore, string-length($textbefore))"/>
  &lt;xsl:variable name="nextchar"
       select="substring($textafter, 1, 1)"/>

  &lt;!-- Now the action: -->
  &lt;xsl:if test="$prevchar != normalize-space($prevchar)">
  &lt;!-- If the original text had a space before, add one back -->
    &lt;xsl:text> &lt;/xsl:text>
  &lt;/xsl:if>

  &lt;xsl:copy>
  &lt;!-- Copy the element over -->
    &lt;xsl:copy-of select="@*"/>
    &lt;xsl:apply-templates/>
  &lt;/xsl:copy>

  &lt;xsl:if test="$nextchar != normalize-space($nextchar)">
  &lt;!-- If the original text had a space after, add one back -->
    &lt;xsl:text> &lt;/xsl:text>
  &lt;/xsl:if>

&lt;/xsl:template>

&lt;/xsl:stylesheet>
</programlisting>
<para>Using David's test:
</para>
    <programlisting>&lt;x>
&lt;para>Some    text    &lt;em>some    other   text&lt;/em>   remaining text&lt;/para>
&lt;para>Some    text&lt;em>    some    other   text&lt;/em>   remaining text&lt;/para>
&lt;para>Some    text    &lt;em>   some    other   text&lt;/em>   remaining
text&lt;/para>
&lt;para>Some    text    &lt;em>some    other   text &lt;/em>   remaining text&lt;/para>
&lt;para>Some    text    &lt;em>some    other   text &lt;/em>remaining text&lt;/para>
&lt;para> Some    text    &lt;em>some    other   text&lt;/em>   remaining text
&lt;/para>
&lt;/x>

We get output (using Saxon)
&lt;x>
   &lt;para>Some text &lt;em>some other text&lt;/em> remaining text&lt;/para>
   &lt;para>Some text&lt;em>some other text&lt;/em> remaining text&lt;/para>
   &lt;para>Some text &lt;em>some other text&lt;/em> remaining text&lt;/para>
   &lt;para>Some text &lt;em>some other text&lt;/em> remaining text&lt;/para>
   &lt;para>Some text &lt;em>some other text&lt;/em>remaining text&lt;/para>
   &lt;para>Some text &lt;em>some other text&lt;/em> remaining text&lt;/para>
&lt;/x>
</programlisting>
<para>If you wanted to get space before the &lt;em> element in the second case or
after in the fifth case, the logic could be extended to catch them (left as
an exercise :-).
</para>

   </answer>
  </qandaentry>





    <qandaentry>
   <question>
    <para>Seperating in-line elements</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison </para>
    <programlisting format="linespecific">

I'm trying to convert something like
&lt;P>Text1&lt;H1>Head1&lt;/H1>Text2&lt;H1>Head2&lt;/H1>Text3&lt;/P>

to 

&lt;P>Text1&lt;/P>
&lt;H1>Head1&lt;/H1>
&lt;P>Text2&lt;/P>
&lt;H1>Head2&lt;/H1>
&lt;P>Text3&lt;/P>

	</programlisting>

<para>I'll talk through your solution, which will hopefully show you why it's
going wrong, and then give you an alternative approach.</para>

	<programlisting>>I tried something like 
>&lt;xsl:template match="P">
> &lt;xsl:for-each match=".">
>  &lt;xsl:chose>
>   &lt;xsl:when test="H1">
>    &lt;H1>&lt;xsl:apply-templates>&lt;/H1>
>   &lt;/xsl:when>
>   &lt;xsl:otherwise>
>    &lt;P>&lt;xsl:apply-templates>&lt;/P>
>   &lt;/xsl:otherwise>
>  &lt;xsl:chose>
> &lt;/xsl:for-each>
>&lt;/xsl:template>
</programlisting>

<para>There are three errors that your XSLT processor should have picked up on:</para>

<simplelist>
	  <member>1. the 'match' attribute on the xsl:for-each should be a 'select' attribute</member>
	  <member>2. the 'xsl:chose' element should be called 'xsl:choose'</member>
	  <member>3. the xsl:apply-templates elements should be closed</member>
	  <member>4. the xsl:choose end tag is missing a '/'</member>
	</simplelist>

<para>After making those changes, what this template says is:</para>

<para>When you find a 'P' element, then for each current node, if it contains an
H1 element, then create an H1 element and make its contents the result of
applying templates to the children of this element, and if not then make a
P element with the contents being result of applyign templates to the
children of this element.</para>

<para>There are a few things wrong with that.  Firstly, you probably want to
iterate over the *contents* of the P element, not the P element itself, in
other words:</para>
	<programlisting>
  &lt;xsl:for-each select="node()">
    ...
  &lt;/xsl:for-each>
</programlisting>
<para>Secondly, within this xsl:for-each, you want to test whether the current
node (which is either a text node or an H1 element) *is* an H1 element.
The test="H1" tests whether the current node has an H1 element *as a
child*.  You should use:</para>

	<programlisting>  &lt;xsl:when test="self::H1">
    ...
  &lt;/xsl:when>
</programlisting>

<para>instead.</para>

<para>Finally, within the xsl:for-each, if the node is a text node (e.g.
'Text1'), then rather than applying templates to its children (it doesn't
have any), you want to get its value:</para>

	<programlisting>  &lt;xsl:otherwise>
    &lt;xsl:value-of select="." />
  &lt;/xsl:otherwise>

If you make these changes, you get:

&lt;xsl:template match="P">
 &lt;xsl:for-each select="node()">
  &lt;xsl:choose>
   &lt;xsl:when test="self::H1">
    &lt;H1>&lt;xsl:apply-templates />&lt;/H1>
   &lt;/xsl:when>
   &lt;xsl:otherwise>
    &lt;P>&lt;xsl:value-of select="." />&lt;/P>
   &lt;/xsl:otherwise>
  &lt;/xsl:choose>
 &lt;/xsl:for-each>
&lt;/xsl:template>
</programlisting>
<para>which creates the desired output in Saxon and Xalan.</para>
<para>
However, an iterative approach is generally not the best approach to take
when you're faced with mixed content.  Rather than thinking in terms of
iterating over the contents of the P element, you can let the processor
fall back on built in rules that apply templates to its content
automatically.  Then you can construct rules (templates) that tell the
processor what to do when it comes across, within a P element, either an H1
or a text() node, or anything else.</para>
<para>
So, you can use the following template to say 'whenever there's a text()
node within a P element, create a P element to hold its content':</para>
	<programlisting>
&lt;xsl:template match="P/text()">
  &lt;P>&lt;xsl:value-of select="." />&lt;/P>
&lt;/xsl:template>
</programlisting>
<para>And the following template to say 'whenever there's an H1 element within a
P element, create an H1 element with the content being the result of
applying templates to its content':</para>
	<programlisting>
&lt;xsl:template match="P/H1">
  &lt;H1>&lt;xsl:apply-templates />&lt;/H1>
&lt;/xsl:template>
</programlisting>


   </answer>
  </qandaentry>



  <qandaentry>
   <question>
    <para>Element name normalisation</para>

   </question>
   <answer>
    <para role="author">Wendell Piez </para>
   
<para>Sometimes SGML tools will do things that mess with tag names,
such as folding them uniformly into upper or lower case. This
is a pain because they must be switched back into their original
forms (their DTD-normalized forms) to be validated as XML, if you
want to move the file back into an XML environment. If their
proper forms happen to include mixed-case names, you're in a
hole. The perfect solution would be a DTD-aware process.
A less perfect approach is to provide the correct names in an
alternate spec of some sort. Since unextended XSLT has no
access to the DTD's model, this stylesheet takes the latter
approach.</para>
<para>
This stylesheet is wired to run with a configuration file
'mlangnames.xml' as its listing of names (element names,
attribute names and names of enumerated attribute values);
but you can override that either by changing the stylesheet or
by setting by switching the 'names' parameter on the command
line, thus normalizing to the tag set of your choice.</para>
<para>
So, for example, a routine invoking the Saxon processor could run:</para>
	<programlisting>
Saxon -o fixed.xml messedup.xml xmlnames.xsl names=mynames.xml
</programlisting>
	<para>See <ulink url="mlangnames.xml">mlangnames.xml</ulink> for the format of the names lists and <ulink url="xmlnames.xsl">this</ulink> is the stylesheet</para>
   </answer>
  </qandaentry>



    <qandaentry>
      <question>
	<para>Normalise space</para>
      </question>
      <answer>
	<para role="author">Jeni Tennison</para>
	<literallayout>

>> For the first state (Alabama), "admissions/state/text()"
>> evaluates to something like:
>> 
>> Alabama&lt;cr>&lt;space>&lt;space>&lt;cr>&lt;space>&lt;space>
>> 
>> Which is NOT the same as:
>> 
>> Alabama
>> 
	</literallayout>

<para>normalize-space() strips leading and trailing space, so if the string
was:</para>

	<literallayout>  "Alabama&lt;cr>&lt;space>&lt;space>&lt;cr>&lt;space>&lt;space>"</literallayout>

<para>then all that trailing space would be stripped and you'd get:</para>

	<literallayout>  "Alabama"</literallayout>

<para>It's only spaces in the *middle* of the string that get collapsed down
to a single string. So for example:</para>

	<literallayout>  "New&lt;cr>&lt;space>&lt;space>&lt;cr>&lt;space>&lt;space>York"</literallayout>
<para>
would become:</para>

	<literallayout>  "New York"</literallayout>


      </answer>
    </qandaentry>

 </qandaset>
</webpage>


<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
