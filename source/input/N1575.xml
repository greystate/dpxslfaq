<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-08-15 09:51:13 dpawson"   -->

<!DOCTYPE webpage  SYSTEM "../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="N1575">
<config param="desc" value="Attribute Value Templates"/>
<config param="dir" value="xsl"/>
<config param="filename" value="N1575.html"/>
<head>
<title>Attribute Value Templates</title>
<summary>Attribute Value Templates</summary>
<keywords>Attribute Value Templates</keywords>
<description>Attribute Value Templates, AVTs</description>
</head>
<qandaset>
<qandaentry>
    <question>
<para>Curly brackets and AVTs</para>
</question>
    
    
      
    
    <answer>
<para role="author">Mike Kay + David Carlisle</para>
      <programlisting>
&gt;  I want to say
&gt; &lt;xsl:value-of select="el1/*[position()={$i}]"/&gt;
&gt; 
    </programlisting>
    <programlisting>Write &lt;xsl:value-of select="el1/*[position()=$i]"/&gt;
or &lt;xsl:value-of select="el1/*[number($i)]"/&gt;
</programlisting>
    <para>You should never have curly brackets inside an XPath
expression.
</para>
    <para>[Ednote:]
</para>
    <para>The magic of AVT's ?
</para>
    <para>An Attribute Value Template is:
(in very simple English)
</para>
    <para>An expression inside curly brackets {}. </para>
    <para>E.g. 
</para>
    <programlisting>&lt;xsl:tempalte match ="x"&gt;
  &lt;elem attrib="{some-value}"&gt;
etc.
some-value can be an xpath expression such as path/to/sought/element
or a variable.
</programlisting>
    <programlisting>As Mike says, we can't use it in &lt;xsl:template match=" ..... "&gt;
we can't use it in   &lt;xsl:value-of select=" ...."&gt;
we can't use it in   &lt;xsl:variable select="....."&gt;
</programlisting>


    <para>And nowhere else?
To which David C replied:
</para>

    <para>No.
</para>
    <para>It can only be used in an attribute declared to be an
"Attribute Value template" in the XSLT recommendation.
</para>
    <para>There is no other possible description, the list of which
elements take AVT arguments and which don't is somewhat
arbitrary. You just have to check in each case. But
attributes (select, test etc) that take xpath expressions
are never interpreted as AVT, as Michael said.
</para>
    <para>Tony Graham kindly adds,
</para>
    <para>
The boxed element "prototypes" in the XSLT recommendation
and the similar prototypes in Mulberry's XSLT and XPath
	  Quick Reference (<ulink
	    url="http://www.mulberrytech.com/quickref">mulberrytech</ulink>) use
"{" and "}" around the attribute values that may include
AVTs.
</para>
    <para>
After all, it doesn't make sense to put an AVT in something
that's already going to be evaluated as an expression.
</para>
    </answer>
  </qandaentry>

    <qandaentry>
      <question>
	<para>Using {AVT}'s, Attribute Value Templates</para>
      </question>
      <answer>
	<para role="author">Jeni Tennison</para>
	<literallayout>

> Can someone please explain why I need to use {} bracket in the href
> of xsl:results-document?
</literallayout>
<para>
The href attribute can take a literal value. For example:</para>

	<programlisting>  &lt;xsl:result-document href="index.html">
    ...
  &lt;/xsl:result-document>
</programlisting>
<para>tells the processor to associate the result document with the URI
"index.html".</para>
<para>  
If you want to have the path to the result document you're creating be
dependent on something you compute within the stylesheet then use the
attribute value template:</para>
	<programlisting>
  &lt;xsl:result-document href="{$vFilePath}">
    ...
  &lt;/xsl:result-document>
</programlisting>
<para>The XPath expression within the {}s is evaluated and the string value
is inserted into the attribute value. Then the attribute value is used
as normal. So if $vFilePath had the value 'index.html', this would be
exactly the same as using 'index.html' literally as above.</para>
	<programlisting>
> I'm familiar with using {} as a shortcut for writing values into
> output attributes but I'm not sure why I can't do something like:
>
> &lt;xsl:result-document href="string($vFilePath) ...
</programlisting>
<para>This means that the URI associated with the result document is,
literally, "string($vFilePath)". That isn't a legal URI, which is why
Saxon is objecting to it.</para>
	<programlisting>
> Below are some examples of xsk:result-document which have confused
> me.
>         This works..
>
>         &lt;xsl:template match="file">
>                 &lt;xsl:variable 
>              name="vFileName" 
>                 select="concat( 'file:///'
>                      , @path , 
>                       $fileSep , 'test.xml' )"/>
>                 &lt;xsl:result-document href="{$vFileName}"  >
>                         &lt;x>output..&lt;/x>
>                 &lt;/xsl:result-document>
>         &lt;/xsl:template>
</programlisting>
<para>Here, the value of the $vFileName is used as the value of the href
attribute. Since the value of $vFileName is a string which is a legal
URI, this is fine.</para>
	<programlisting>
>         This does not work.... Saxon says : 
>         The system identifier of the principal
>              output file is unknown..
>
>         &lt;xsl:template match="file">
>                 &lt;xsl:variable 
>           name="vFileName" 
>            select="concat( 'file:///'
>                      , @path , 
>                        $fileSep , 'test.xml' )"/>
>                 &lt;xsl:result-document href="$vFileName"  >
>                         &lt;x>output..&lt;/x>
>                 &lt;/xsl:result-document>
>         &lt;/xsl:template>

</programlisting>

<para>Here, the literal string "$vFileName" is used as the URI. That isn't a
valid URI, so Saxon objects to it.</para>

	<programlisting>>         This works..
>
>         &lt;xsl:template match="file">
>
>                 &lt;xsl:result-document
>                      href="{'file:///'}{@path}{$fileSep}{'test.xml'}"  >
>                         &lt;x>output..&lt;/x>
>                 &lt;/xsl:result-document>
>         &lt;/xsl:template>
</programlisting>
<para>Each XPath expression is evaluated and inserted in place. The result
is a legal URI, so Saxon does not object. It would be exactly the same
if you used:</para>
	<programlisting>
  &lt;xsl:result-document href="file:///{@path}{$fileSep}test.xml">
    ...
  &lt;/xsl:result-document>
</programlisting>
<para>There's no need to use {}s for literal strings.</para>
<para>


Basically, if you put it in {}s it gets evaluated. If you don't, it
gets treated as a literal string.</para>


      </answer>
    </qandaentry>



  


    <qandaentry>
      <question>
	<para>AVT with pass-through</para>
      </question>
      <answer>
	<para role="author">Tom Passin</para>
	<literallayout>

How to quote "${abc}" so it will pass through without
evaluation?

>  I can't figure out how to specify "${abc}" in attribute 
> values in my template so it will just generate "${abc}" and will not 
> try to evaluate it.
</literallayout>

<para>Double the braces -</para>
	<programlisting>
{{$abc}}
</programlisting>


      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>for-each inside an attribute value template</para>
      </question>
      <answer>
	<para role="author">Michael Kay </para>
	<literallayout>


> I need to pass parameters in a URL like: &lt;a 
> href="page.jsp?param1=x&amp;param2=y" /> The number of parameters is 
> unknown.
</literallayout>

<para>
In XPath 2.0 you could use an XPath expression in your AVT containing a for
expression, something like</para>
<programlisting>
href="{string-join(for $x in //param return concat(keyword, '=', value),
'&amp;amp;')}"</programlisting>
<para>
In 1.0, don't use AVTs, use xsl:attribute</para>
<programlisting>
&lt;xsl:attribute name="href">
  &lt;xsl:for-each>

  &lt;/xsl:for-each>
&lt;/xsl:attribute>
</programlisting>
      </answer>
    </qandaentry>

</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
