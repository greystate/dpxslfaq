<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2002-04-21 09:19:49 Dave Pawson"   -->
<!DOCTYPE webpage  SYSTEM "../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="generic">

<head>
<title>Generic Template</title>
<summary>Generic template</summary>
  <keywords>generic xslt</keywords>
</head>
<qandaset>

 
  <qandaentry>
   <question>
    <para>Dynamic Functions article.</para>

   </question>
   <answer>
    <para role="author">Dimitre Novatchev.</para>
  

<para>"Dynamic Functions using FXSL: Composition, Partial Applications and
	  Lambda Expressions" at <ulink url="http://www.topxml.com/xsl/articles/df">topxml.com</ulink></para>



<para>This one is at least as important (if not more) than the first article
on functional programming in XSLT. I think it will be most useful for
people, who are interested in further understanding and using FP in
XSLT.</para>
<para>
Contents:</para>

	<simplelist>
	  <member>Introduction</member>
	  <member>  1. Functional composition, curried functions, partial applications
and lambda expressions.</member>
	  <member>  2. Implementation of functional composition in XSLT. Examples.</member>
	  <member>  3. Implementation of currying and partial application in XSLT.</member>
	  <member>  4. Using currying and partial application - the iter and power
functions.</member>
	  <member>  5. Creating a new function dynamically - the calculator-store
problem.</member>
	  <member>Conclusion</member>
</simplelist>
<para> two excerpts:</para>
<para>
"This article is a follow-up from the recent publication "The
Functional Programming Language XSLT". Provided here is the answer to a
FAQ about functional programming in XSLT: Can functions be created
dynamically during run-time and what are some general ways to achieve
this?. Using the XSLT functional programming library FXSL, examples are
given of performing functional composition, currying and partial
application of functions, as well as of dynamically creating a
function, which corresponds to a lambda expression."</para>
<para>
"This article demonstrated and explained the details of an XSLT
implementation of three major ways of dynamically creating functions
and using them:</para>
<simplelist type="vert">
	  <member> - Single and multiple function composition. </member>
	  <member> - Currying of functions and partial application of curried functions. </member>
	  <member> - Lambda expressions.</member> </simplelist>
<para>
The XSLT implementation of partial application is more powerful than
the Haskell one, because it allows binding of arguments regardless of
order. At the same time partial application in XSLT is very convenient,
using the traditional XSLT way of specifying arguments through
xsl:with-param.</para>
<para>
Currying and partial application support has been included in the
latest release of the functional programming library FXSL.</para>
<para>
Also demonstrated was a general way to model and implement in XSLT
objects having inheritance and virtual functions."</para>
   </answer>
  </qandaentry>




 
  <qandaentry>
   <question>
    <para>Generic templates</para>

   </question>
   <answer>
    <para role="author">Dimitre Novatchev</para>
    <para>

If a template must be instantiated (based on the value of $someVar) using
xsl:choose, this means that the names/modes of all such templates must be known in
advance.
Every time we add a new template to be called, the calling template's xsl:choose
must be updated to incorporate the new case.</para>

<para>Obviously this static invocation scheme is hardly convenient.</para>

<para>Actually there is a method for building generic templates so that they can cause
different templates to be istantiated dynamically and without using xsl:choose.</para>
<para>
It is not necessary to know anything in advance about the templates to be
istantiated, and in fact they may be written long after the generic template.</para>
<para>
The idea is to pass to the generic template a separate parameter, which uniquely and
***dynamically*** identifies the template that will be instantiated. This parameter
is of type node-set (single node), that has a unique namespace-uri.</para>
<para>
The caller of the generic template may define many different "action templates" that
match only specific unique types of nodes. During the execution of a stylesheet a
generic template may be called many times with different "action template types" to
perform similar processing.</para>
<para>
Below is an example of a generic template that finds and returns the "maximum" node
in a node-set. The meaning of "maximum" is determined completely by an external
template, which implements the relation "greater". A "template type" parameter is
passed to the generic template, so that it can dynamically invoke the necessary
"greater"-action template.</para>
<para>
In this example there are two "greater"-action templates implemented -- one that
uses the traditional ">" relation for numbers, and another that compares their
reciprocal values.</para>
<para>
There could be potentially unlimited number of different "greater"-action templates
- -- e.g. comparing distance, space, perimeter,... etc.</para>
<para>
They can all be passed to a single generic "maximum" template and it will always
find the correct maximum node.</para>
<para>
The same method can be used for a generic sort template and in the implementation of
many other useful generic algorithms. </para>
<para>
This method does't have the problem of "the last overrides override all" (described
a few months ago by Jeni) when there are several templates overriding some templates
of an imported stylesheet.</para>
<para>
Genericity is an extremely useful feature -- at present it is missing from both the
EXSLT site and the Standard XSLT Library site. </para>
<para>
In my opinion in order for a standard template library to be really useful, it must
contain ***generic*** templates. Thus it will have an unlimited number of
applicationsas opposed to a fixed, very small number of applications. </para>
<para>
In the near future I will provide some initial set of useful generic templates --
min, max, sort, partial sort, binary search,... etc.</para>
<para>

Here's the example:</para>

<para>XML source document:</para>

	<programlisting>&lt;numbers>
 &lt;num>1&lt;/num> 
 &lt;num>3&lt;/num> 
 &lt;num>2&lt;/num> 
 &lt;num>9&lt;/num> 
 &lt;num>4&lt;/num> 
 &lt;num>6&lt;/num> 
 &lt;num>5&lt;/num> 
 &lt;num>7&lt;/num> 
 &lt;num>8&lt;/num> 
&lt;/numbers></programlisting>

<para>Stylesheet:</para>

	<programlisting>&lt;xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:num="num"
xmlns:num2="num2"
>

  &lt;num:node/>
  &lt;num2:node/>

  &lt;xsl:output omit-xml-declaration="yes" />
  
  &lt;xsl:variable name="gtNum-node" select="document('')//num:*"/>
  &lt;xsl:variable name="gtNum2-node" select="document('')//num2:*"/>

  &lt;xsl:template match="/">
    &lt;xsl:call-template name="get-max">
      &lt;xsl:with-param name="greaterSelector" select="$gtNum-node" />
      &lt;xsl:with-param name="nodes" select="/numbers/num" />
    &lt;/xsl:call-template>
    
    &lt;xsl:text>&amp;#xA;&lt;/xsl:text>

    &lt;xsl:call-template name="get-max">
      &lt;xsl:with-param name="greaterSelector" select="$gtNum2-node" />
      &lt;xsl:with-param name="nodes" select="/numbers/num" />
    &lt;/xsl:call-template>
  &lt;/xsl:template>

  &lt;xsl:template name="get-max">
    &lt;xsl:param name="greaterSelector" select="/*"/>
    &lt;xsl:param name="nodes" />
    &lt;xsl:choose>
      &lt;xsl:when test="$nodes">
        &lt;xsl:variable name="max-of-rest">
          &lt;xsl:call-template name="get-max">
            &lt;xsl:with-param 
	  name="greaterSelector" select="$greaterSelector" />
            &lt;xsl:with-param name="nodes"
            select="$nodes[position()!=1]" />
          &lt;/xsl:call-template>
        &lt;/xsl:variable>

        &lt;xsl:variable name="isGreater">
         &lt;xsl:apply-templates select="$greaterSelector" >
           &lt;xsl:with-param name="n1" select="$nodes[1]"/>
           &lt;xsl:with-param name="n2" select="$max-of-rest"/>
         &lt;/xsl:apply-templates>
        &lt;/xsl:variable>
        
        &lt;xsl:choose>
          &lt;xsl:when test="$isGreater = 'true'">
            &lt;xsl:value-of select="$nodes[1]" />
          &lt;/xsl:when>

          &lt;xsl:otherwise>
            &lt;xsl:value-of select="$max-of-rest" />
          &lt;/xsl:otherwise>
        &lt;/xsl:choose>
      &lt;/xsl:when>

      &lt;xsl:otherwise>
        &lt;xsl:value-of select="-999999999" />

&lt;!-- minus infinity -->
      &lt;/xsl:otherwise>
    &lt;/xsl:choose>
  &lt;/xsl:template>
  
  &lt;xsl:template name="isGreaterNum" 
	  match="node()[namespace-uri()='num']">
    &lt;xsl:param name="n1"/>
    &lt;xsl:param name="n2"/>

    &lt;xsl:value-of select="$n1 > $n2"/>
  &lt;/xsl:template>

  &lt;xsl:template name="isGreaterNum2" 
	  match="node()[namespace-uri()='num2']">
    &lt;xsl:param name="n1"/>
    &lt;xsl:param name="n2"/>

    &lt;xsl:value-of select="1 div $n1 > 1 div $n2"/>
  &lt;/xsl:template>

&lt;/xsl:stylesheet>
</programlisting>


<para>The result produced is the two correct maximums:</para>

	<programlisting>9
1
</programlisting>

   </answer>
  </qandaentry>
 <qandaentry>
   <question>
    <para>dynamically calling templates</para>

   </question>
   <answer>
    <para role="author">Dimitre Novatchev</para>
    <programlisting format="linespecific">

</programlisting>

	<para>I have submitted to the <ulink url="http://www.exslt.org">EXSL</ulink> list a number of generic templates
 implementing generic functions as specified in the subject.</para>

<para>One of the neat ideas in these implementations is a possible answer to
the frequently asked question "how do I call templates dynamically?" -
how to choose the name of the template that you want to call based on
a variable.</para>

<para>One answer is to add a match pattern to the template that matches a
(single) node that acts as a proxy or UID for the template.  Dimitre's
examples use a specially-defined node at the top level of the
stylesheet.  For named templates, another approach would be to use the
xsl:template elements themselves, e.g.:</para>

	<programlisting>&lt;xsl:template name="my:named-template"
              match="xsl:template[@name = 'my:namedTemplate']">
   ...
&lt;/xsl:template>
</programlisting>
<para>You can then apply templates to the relevant xsl:template element to
have the same kind of effect as calling the template.  I would
probably set up a global variable to hold the template elements:</para>

	<programlisting>&lt;xsl:variable name="templates" select="document('')/xsl:template" /></programlisting>

<para>[Note: this only gets the templates in the current physical
stylesheet, not any that have been imported/included, which could be a
big drawback - is this something that could be addressed with the
closure() function that Christian Nentwich suggested?]</para>

<para>And then filter that to get the relevant one:</para>

	<programlisting>  &lt;xsl:apply-templates select="$templates[@name = $template-name]" /></programlisting>

<para>[Note: you could use the template elements as nodes with Dimitre's
generic templates, but they'd be passed as a parameter rather than
have templates applied to them.]</para>
  
<para>The big difference between applying templates in this way and calling
the template is that the current node and position() within the
template body will be the xsl:template element and 1 respectively.
You can get around this problem by passing in parameters holding these
values in the context in which the template is called/applied:</para>

	<programlisting>  &lt;xsl:apply-templates select="$templates[@name = $template-name]">
     &lt;xsl:with-param name="current" select="." />
     &lt;xsl:with-param name="position" select="position()" />
  &lt;/xsl:apply-templates>
</programlisting>  
<para>I'm not sure if this is new, but it's not something that we've been
giving as an answer to the 'dynamically calling templates' FAQ, so I
thought it would be worth describing.</para>
   </answer>
  </qandaentry>


 </qandaset>
</webpage>


<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
