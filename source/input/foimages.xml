<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-10-23 05:36:17 dpawson"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="foimages">
<head>
<title>Images</title>
<summary>fo:external-graphics</summary>
  <keywords>images, Images</keywords>
</head>
<qandaset>


 



    <qandaentry>
      <question>
	<para>Loading a graphic from a file</para>
      </question>
      <answer>
	<para role="author">Eliot Kimber</para>
	

	
<para>There is no simple single solution to this problem because it
depends entirely on the relative locations of the FO instance (if you
are generating an instance) and the graphic *at the time you render
them*, not at the time you generate the FO.</para>
<para>
Therefore, for different processing environments, it might be most
appropriate to generate an absolute path and in others you have to
generate a relative path relative to some pre-defined location.</para>
<para>
In addition, doing path processing is generally easier in a language
like Java than in XSLT (although you can do it in XSLT, of
course). For example, we have utility Java libraries that do things
like compare two paths and return the shortest relative path. We then
expose these through Saxon extension functions so that in the XSLT we
can easily generate relative paths to graphics given some base path
(normally passed in as a parameter to the XSLT process).</para>
<para>
For one customer we have to provide an FO-generation-time option of
whether graphic paths are relative or absolute because different users
of the code have different business rules.</para>
<para>
Finally, remember that relative paths will be relative to the location
of the FO instance, not the original XML document (unless you set the
xml:base attribute in the FO instance to be the location of the
orignal XML document), which can be a problem, especially if you
generate the FO instance and then move it somewhere else before
rendering it.</para>
<para>
This is all presuming that your graphics are not managed in some
URL-accessible content store that would allow you to specify
location-independent absolute URIs. In essence, all the W3C
specifications assume that this is the case, even though for most
users it is never the case.</para>


      </answer>
    </qandaentry>




  <qandaentry>
   <question>
    <para>Referring to images </para>

   </question>
   <answer>
    <para role="author">David Carlisle</para>
     <literallayout format="linespecific" class="normal">

   > &lt;fo:external-graphic  
    src="file:../graphics/page.gif" content-height="200%"
   > content-width="200"/>
	</literallayout>


<para>Also, (unfortunately) XSL for no particularly good reason
insists that the URI is surrounded by url( ... )
so that should be  src="url(file:///......)"</para>
<para>
(note that you can not put a relative uri like ../graphics/page.gif
after file: (or any other URI-scheme prefix) Either you should have a
relative URI with no prefix, or an absolute URI, this is a URI syntax
issue, nothing to do with XSL).</para>
	<para>Note that the WG shuold be clarifying this in the version 1 errata very soon (July 2002)</para>

   </answer>
  </qandaentry>
 

 


   


    <qandaentry>
   <question>
    <para>How to insert images into an fo file</para>

   </question>
   <answer>
    <para role="author">??</para>
    <para>
This is the xsl for images 
&lt;!-- IMAGES -->
&lt;xsl:template match="imagedata">
   &lt;fo:block>
       &lt;fo:inline-graphic content-height="auto" 
	    content-width="auto" href="file:{@fileref}"/>
   &lt;/fo:block>
&lt;/xsl:template>

</para>
   </answer>
  </qandaentry>



  <qandaentry>
   <question>
    <para>Graphics in xsl-fo</para>

   </question>
   <answer>
    <para role="author">David Tolpin</para>
     <literallayout format="linespecific" class="normal">


  > I have a question concerning XSL-FO. It's about 
  > &lt;fo:external-graphic>. I
  > have a JPEG of unknown size and want to place it in the 
  > horizontal middle of
  > the page (align="center"). But neither I get the correct 
  > scaling nor the
  > JPEG centered. I found the attribute "display-align", but that's not
  > implemented in FOP. With XEP I don't get any picture.
  
  &lt;fo:block 
        align="center">&lt;fo:external-graphic src="1.jpg"/>
&lt;/fo:block>
  </literallayout>
 
	<literallayout>  
> I tried &lt;fo:external-graphic src="1.jpg" width="22.7cm" 
  height="12.4cm"/>
  > and the not implemented attributes content-width="scale-to-fit",
  > content-height="scale-to-fit" and scaling="uniform".
</literallayout>  
  <para>Try</para>
  
	<literallayout>  &lt;fo:external-graphic src="1.jpg" content-width="22.7cm" 
 content-height="12.4cm" scaling="uniform"/></literallayout>
   </answer>
  </qandaentry>


 
    <qandaentry>
   <question>
    <para>Image in a table, with padding</para>

   </question>
   <answer>
    <para role="author">Nikolai Grigoriev</para>
    <programlisting format="linespecific">


> &lt;fo:table-cell padding-left="7cm">&lt;fo:block>&lt;fo:inline-graphic height="3cm"
> width="3cm"
>          href="&lt;xsl:value-of select="header/headerimage/url"
> />"/>&lt;/fo:block>&lt;/fo:table-cell>
>
> But this does not seem to be legal syntax. Does somebody know how to do
> this?</programlisting>

<para>Assumptions:</para>
<para>
1. You're writing a stylesheet, and the URL to the image is contained in the XML
document to be processed, enclosed in an &lt;url> element which is contained in a
&lt;headerimage> element which is contained in a &lt;header> element which is a child
of the current node, and all these elements have no siblings of the same name.
(;-)).</para>
<para>
2. You're using an old XSL FO version (1999-04-21) - either FOP version 13.0 or
earlier, or XEP.</para>
<para>
Then, the syntax would be:</para>
	<programlisting>
&lt;fo:table-cell padding-left="7cm">
  &lt;fo:block>
    &lt;fo:inline-graphic height="3cm" width="3cm"
                 href="{header/headerimage/url}"/>
  &lt;/fo:block>
&lt;/fo:table-cell>
</programlisting>
<para>By the way: in XSL FO 1999-04-21, you could use a fo:display-image (which is a
block-level element), inserting it directly into an fo:table cell without an
intercalary fo:block:</para>
	<programlisting>
&lt;fo:table-cell padding-left="7cm">
    &lt;fo:display-graphic height="3cm" width="3cm"
                 href="{header/headerimage/url}"/>
&lt;/fo:table-cell>
</programlisting>

   </answer>
  </qandaentry>

 

  <qandaentry>
   <question>
    <para>Images and XSL FO</para>

   </question>
   <answer>
    <para role="author">David Tolpin</para>
    <programlisting format="linespecific">

> > The pdf-file has to look as followed:
> > 		_________________
> > 	text 1	|	image	     |
> > 	text 2	|		     |
> > 	text 2	|		     |
> > 	text 2	|________________|
> > 	text 2 text2 text 2
> 
> I've tried, and failed, to get text to wrap round an image.
	</programlisting>
<para> Floats (fo:float), floating horizontally are for that. Unfortunately,
in most implementations I'm aware of floats either do not float at all,
or float in the natural vertical direction.</para>
<para>
Other implementation do support horizontal floating, but lack other significant
features.</para>
<para>
And no, RenderX XEP does not handle fo:float yet. (01 2001)</para>
<para>
Sebastian Queries</para>
	<programlisting>
>taking that as a challenge (:-}), I looked again at the spec. am I
>right in the following crude view?
>
> &lt;fo:float float="none">...      stays exactly where it is
> &lt;fo:float float="left">...      sits on the left side of the
>                                paragraph,  and text floats around it
> &lt;fo:float float="right">...     sits on the right side of the
>                                paragraph,  and text floats around it
> &lt;fo:float float="before">...    goes to top of this, or subsequent pages
>
</programlisting>
<para>Paul Grosso answers:</para>
<para>
Yes, you are basically correct.  So when fo:float's float property
is "before", you get what TeX would call a topfloat.</para>

	<programlisting>>this appears to exclude floats going to the *bottom* of the page, if I
>understand it right. surely this is unnaturely restrictive??? what if
>the float looked better at the bottom of the page?
</programlisting>

<para>Correct.  The theory of floats is complex (anyone familiar with the
details of TeX's inserts and such, as I know Sebastian is, can
appreciate this, and what TeX does with floats is less than half
of the actual complexity), and the XSL WG decided we'd never get
the XSL spec out if we didn't make some simplifying assumptions
in this area (the other alternative was to leave vertical floats
out of XSL 1.0 altogether, and we didn't want to do that).  So we
allowed for top floats only (and footnotes, but via the fo:footnote
FO) in XSL 1.0. </para>


	<programlisting>>its also odd that "&lt;fo:float>" defaults to float="none", and so does
>not float at all.....
</programlisting>
<para>The CSS default is none, and we just copied it.  Furthermore, since
the value of the float property could make for either a vertical
float (aka insert) or a horizontal "float" (aka wrap-around text)
which are very different things, it would be hard to have a default
that was useful and intuitive in the majority of cases.  Making the 
default none just means that, when you explicitly insert an fo:float, 
you also have to explicitly set the float property to something.</para>

   </answer>
  </qandaentry>


    <qandaentry>
      <question>
	<para>Text wrap round image</para>
      </question>
      <answer>
	<para role="author">Ken Holman</para>
	<literallayout>

>    But I want, let say if the attribute"align" in my image tag
>is left, then first it should place the image on the left and leave
>2mm space and start the writing the content on the right side.
>once the content height reached the height of the image then from
>next line content should start from the beginning of the pagelike a
>full line. i should be like a flow.
>   Am I clear?
</literallayout>
<para>
Now you are ... and you just need to use side-floats for that.</para>

	<literallayout>>   I think even this happens with list-block also because we are
>creating two blocks . I want a block which contains both content and
>image not two separate blocks.
</literallayout>
<para>Now I can tell from your wording what you want.</para>

	<programlisting>

&lt;block xmlns="http://www.w3.org/1999/XSL/Format">
  &lt;float float="start">&lt;external-graphic src="image"/>&lt;/float>
  Text goes here.
&lt;/block>
</programlisting>
<para>The following illustrates this in both Antenna House and XEP using just 
text in the float instead of a graphic:</para>
	<programlisting>
&lt;fo:block>
   &lt;fo:float float="start">
     &lt;fo:block background-color="silver">F111&lt;/fo:block>
     &lt;fo:block background-color="silver">F111&lt;/fo:block>
   &lt;/fo:float>
  B222 B222 B222 B222 B222 B222 B222 B222 B222 B222 B222 B222 B222
&lt;/fo:block>
</programlisting>

      </answer>
    </qandaentry>


 <qandaentry>
   <question>
    <para>Scaling images</para>

   </question>
   <answer>
    <para role="author">Max Froumentin</para>
     <literallayout format="linespecific" class="normal">

> I am using fo:external-graphic to display images.I have a requirement
> to show the same image in different sizes.Is there any way to scale
> images in XSL:FO??
	</literallayout>
<para>Yes, the propreties "content-height" and "content-width" apply to 
fo:external-graphic. The scaling is controlled by the scaling and
the scaling-method properties. See 6.6.5 fo:external-graphic
for more details.</para>
   </answer>
  </qandaentry>

  <qandaentry>
   <question>
    <para>Keeping an image with associated text</para>

   </question>
   <answer>
    <para role="author">Ken Holman</para>
     <literallayout format="linespecific" class="normal">

>Does anybody know how to prevent an external-graphic and its following
>text from breaking a line ?
	</literallayout>
<para>Use keep-with-next= on the graphic or keep-with-previous= on the text, or 
keep-together= on the pair.</para>
<para>
The following illustrates this to me on Antenna House (your margins may 
vary, play with the length of the XX prefix to cause the first one to break 
the line between the graphic and the subsequent text:</para>
	<programlisting>
   &lt;block>XXXXX: This is a test with a graphic followed by text 
&lt;external-graphic src="url('smflags.bmp')"/>&lt;inline>hello 
world&lt;/inline>&lt;/block>
   &lt;block>XXXXX: This is a test with a graphic followed by text 
&lt;external-graphic src="url('smflags.bmp')" 
keep-with-next="always"/>&lt;inline>hello world&lt;/inline>&lt;/block>
   &lt;block>XXXXX: This is a test with a graphic followed by text 
&lt;external-graphic src="url('smflags.bmp')"/>&lt;inline 
keep-with-previous="always">hello world&lt;/inline>&lt;/block>
   &lt;block>XXXXX: This is a test with a graphic followed by text &lt;inline 
keep-together="always">&lt;external-graphic src="url('smflags.bmp')"/>hello 
world&lt;/inline>&lt;/block>
</programlisting>

   </answer>
  </qandaentry>


 <qandaentry>
   <question>
    <para>Image sizing</para>

   </question>
   <answer>
    <para role="author">Ken Holman</para>
     <literallayout format="linespecific" class="normal">
>I want te get the width of an image, and if the image has a width greater as
>144 pixels, I want to scale the image, else if the size &lt;= 144 pixels then
>I'd like to display the image at the normal size.
	</literallayout>

<para>
Because the XSL-FO interpretation is arms-length from the XSLT 
transformation, one must always express the desired conditional processing 
in the stylesheet before the XSL-FO processor acts on the resulting 
transformation.  An example of such conditional processing is the set of 
"keep" properties.</para>
<para>
These needs in particular cannot be expressed in the conditional processing 
facilities in XSL-FO, so you must resort to a scheme outside of the 
Recommendation.</para>
<para>
If I had a program that read the dimensions of an image and wrote out a 
small XML instance capturing these measurements, then I would read those 
XML instances at transformation time using the document() function when 
processing the figures and determine in my XSLT stylesheet which strategy 
to follow for the presentation of each image.</para>

   </answer>
  </qandaentry>


    <qandaentry>
      <question>
	<para>fo:external-graphic syntax</para>
      </question>
      <answer>
	<para role="author">Ken Holman</para>
	<literallayout>

>The src attribute is defined as containing a &lt;uri-specification>
>which has the format 'url(' followed by a URI reference in single
>or double quotes, followed by ')'. So your code should be
>
>&lt;xsl:element name="fo:external-graphic">
>   &lt;xsl:attribute name="src">url('&lt;xsl:value-of 
> select="imgurl"/>/zenith.gif')&lt;/xsl:attribute>
	</literallayout>
<para>A nuance  is that the use of the single quote 
as the literal in the url() syntax  
may not be the "safest" when you are dealing with arbitrary URL values from 
your XML authors.</para>
<para>
The single quote is a valid character in the URL syntax according to the 
second last paragraph of section 2.2 of RFC1738 ... so I figured that if 
you used url('{@url}') there was a risk that the single quote of the URL 
would get confused with the single quote of the opening literal.  Perhaps I 
am wrong about this risk, since it isn't mentioned in the Recommendation.</para>
<para>
The safest is to use url("{@url}") to ensure there would 
be no confusion with any valid URL value resolved from the author's data.</para>
<para>
Note that for those who quote their attributes, it would be:</para>
	<programlisting>
    src="url(&amp;quot;{@url}&amp;quot;)"
</programlisting>      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>Anchor an image to the bottom of a page</para>
      </question>
      <answer>
	<para role="author">David Tolpin</para>
	<literallayout>
>          &lt;fo:flow flow-name="xsl-region-body">
> 
>              &lt;fo:block>
>              	...
>              &lt;/fo:block>
>              ...
>              &lt;fo:block> &lt;fo:external-graphic src='url(foo.jpg)'/>
&lt;/fo:block>
>        		...
>              &lt;fo:block>
>              	...
>              &lt;/fo:block>
> 
>          &lt;fo:flow>
> 
> 
> The fo:flow element contains material for several pages, and I want the 
> image to be shifted (or anchored) to the bottom of its current page (the 
> current page is the page where the image would appear without special
>action).
</literallayout>
	<programlisting>  &lt;fo:block>
    &lt;fo:footnote>
      &lt;fo:footnote-body>
      &lt;fo:inline/>
        &lt;fo:external-graphic src="url(foo.jpg)"/>
      &lt;/fo:footnote-body>
    &lt;/fo:footnote>
  &lt;/fo:block>
</programlisting>

      </answer>
    </qandaentry>

 <qandaentry>
      <question>
	<para>Full page images</para>
      </question>
      <answer>
	<para role="author">David Tolpin</para>
	<literallayout>

> I have several figures/external-graphics that I need
> to render at full page size.  
> However, I do not want to create a page-break at
> the point in the document flow where the image appears.  
> Rather, I want to place
> the external-graphic on its own page which immediately follows 
> the point in the
> document flow where the fo:external-graphic element appears. 
> The text before
> and after the fo:external-graphic should flow together, 
> uninterrupted by any page breaks.
</literallayout>
<para>
You are talking about before-floats. Just take your images into
&lt;fo:float float="before"> and you will get what you are describing
in a compliant processor</para>
      
      </answer>
    </qandaentry>



    <qandaentry>
      <question>
	<para>Image overlay, e.g. to mark an image as 'draft'.</para>
      </question>
      <answer>
	<para role="author">W. Eliot Kimber</para>
	<literallayout>
>>
>>You can try a block as overlay, using relative positioning.

> Could you please give me further more information? - What do you mean 
> with relative positioning?
</literallayout>
<para>
You need to use block-container or inline-container. For block-container,
the basic pattern is:</para>
	<programlisting>
&lt;fo:block-container>
   &lt;fo:block-container position="absolute">
     &lt;fo:block>This is the first block&lt;/fo:block>
   &lt;/fo:block-container>
   &lt;fo:block-container position="absolute">
     &lt;fo:block>This is the second block. It should overprint the
first&lt;/fo:block>
   &lt;/fo:block-container>
&lt;/fo:block-container>
</programlisting>
<para>The reason this works is that the outer block-container establishes a new
"reference area", that is, an area within which other areas are positioned.</para>
<para>
The two inner block-containers use position="absolute" to indicate that they
are absolutely positioned relative to the boundaries of their containing
reference area. Because both inner block containers are positioned at the
same place (the default position is aligned with the top and left of the
containing reference areas) they overlay each other.</para>
<para>
By default, the Z-order (vertical stacking) is determined by source order,
so the second inner block-container has a higher Z-order than the first
block container.</para>
<para>
The same basic approach works with inline-container, the difference being
that the outer inline-container will be positioned within a sequence of
inline areas rather than as a block, as in the example above. Here is a
sample that works with XSL Formatter 2.5 (in my data set, the graphic is a
little icon about 10pt square):</para>
	<programlisting>
     &lt;fo:block space-before="12pt"
         font-size="12pt"
     >this is before the inline graphic
     &lt;fo:inline-container
         alignment-adjust="10pt"
         inline-progression-dimension="12pt"
     >
       &lt;fo:block-container position="absolute">
         &lt;fo:block>
           &lt;fo:external-graphic src="url(graphics/menu-icon.eps)"/>
         &lt;/fo:block>
       &lt;/fo:block-container>
       &lt;fo:block-container position="absolute">
         &lt;fo:block>
           &lt;fo:leader
               color="red"
               alignment-adjust="middle"
               rule-thickness="1pt"
               leader-length="12pt"
               leader-alignment="reference-area"
               leader-pattern="rule"/>
         &lt;/fo:block>
       &lt;/fo:block-container>
     &lt;/fo:inline-container>
     this is after the inline graphic.
     &lt;/fo:block>
</programlisting>
<para>[It may not work with XEP--I believe XEP implements all
absolutely-positioned blocks relative to the page's reference area. I would
be very surprised if it works with FOP.]</para>
      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>Overlaid graphics</para>
      </question>
      <answer>
	<para role="author">Eliot Kimber</para>
	<literallayout>

> The first of these is that I want to be able to use over-layed text / 
> graphics within the PDF.  For example, lets say I create multi-page 
> PDF customer order confirmations, once the customer confirms they have 
> received the goods, I want to overlay the words "Fulfilled" diagonally 
> across the entire document.
</literallayout>
<para>
If FOP supports block-container you can use an absolutely-positioned block
container to overlay the text. If you want it to be diagonal, you will need
to use a graphic for the text (easy to do with EPS or SVG).</para>
<para>
The one limitation may be support for Z-order--if your FO implementation
doesn't support Z-order or doesn't default to the behavior you want you may
not be able to force the text to overlay the region-body content.</para>

<para>Z-order is the property that determines the vertical stacking over
overlapping areas--a higher Z-order value should be rendered above (on top
of) areas with lower Z-orders.</para>
<para>
Since the overlay would need to be static content (so it could apply to each
page) you'd have to put in into one of the edge regions with the region's
extent set so that it overlaps the region-body. Different FO implementations
may vary in how they place the areas involved, so it will require some
testing to determine if what you want will work with a particular
implementation.</para>
<para>
This problem can also be addressed using a PDF post-process using a tool.
Adding an overlay to every page of a PDF is a pretty simple operation. See
www.pdfzone.com or www.planetpdf.com for a list of likely candidate tools.</para>

      </answer>
    </qandaentry>



    <qandaentry>
      <question>
	<para>Text wrap round image</para>
      </question>
      <answer>
	<para role="author">Ken Holman</para>
	<literallayout>

>    But I want, let say if the attribute"align" in my image tag
>is left, then first it should place the image on the left and leave
>2mm space and start the writing the content on the right side.
>once the content height reached the height of the image then from
>next line content should start from the beginning of the pagelike a
>full line. i should be like a flow.
>   Am I clear?
</literallayout>

<para>Now you are ... and you just need to use side-floats for that.</para>
	<literallayout>
>   I think even this happens with list-block also because we are
>creating two blocks . I want a block which contains both content and
>image not two separate blocks.
</literallayout>
<para>Now I can tell from your wording what you want.</para>

	<literallayout>>   IS there any way to do it in xsl-fo?
</literallayout>
	<programlisting>&lt;block xmlns="http://www.w3.org/1999/XSL/Format">
  &lt;float float="start">&lt;external-graphic src="image"/>&lt;/float>
  Text goes here.
&lt;/block>
</programlisting>
<para>The following illustrates this in both Antenna House and XEP using just 
text in the float instead of a graphic:</para>
	<programlisting>
&lt;fo:block>
   &lt;fo:float float="start">
     &lt;fo:block background-color="silver">F111&lt;/fo:block>
     &lt;fo:block background-color="silver">F111&lt;/fo:block>
   &lt;/fo:float>
  B222 B222 B222 B222 B222 B222 B222 B222 B222 B222 B222 B222 B222
&lt;/fo:block>
</programlisting>      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>Image scaling</para>
      </question>
      <answer>
	<para role="author">G. Ken Holman</para>
	<literallayout>

>Is it possible to scale images so that images that would run off the
>page are scaled to fit, while smaller images retain their existing
>size.
</literallayout>

<para>The pattern I've seen used is:</para>
	  <programlisting>
  &lt;external-graphic url="......."
   width="100%" content-width="scale-to-fit"
   scaling="uniform"
   content-height="100%"/>
</programlisting>
<para>or, if you want to limit by height instead of by width:</para>
<programlisting>
  &lt;external-graphic url="......."
   height="100%" content-height="scale-to-fit"
   scaling="uniform"
   content-width="100%"/></programlisting>
<para>
When images are smaller than the dimension changing specification of the 
content dimension using "scale-to-fit", the image is considered to already 
fit.  If the image is larger than the given dimension, that dimension is 
reduced to fit to the viewport (defined by height= or width=, in the above 
examples, the use of 100% specifies the viewport to be the full dimension 
available, you have to choose one of height or width to be the governing 
dimension since specifying both won't result in proper scaling).</para>
<para>
The scaling="uniform" ensures that the other image dimension is relatively 
scaled so as to not disturb the aspect ratio.</para>
<para>
Finally, the explicit specification of the other image dimension to 100% 
ensures that the governing content dimension specification does not distort 
the content's other dimension ... so images that are smaller than the 
governing dimension do not get stretched to 100% of that dimension's 
available size.</para>
<para>
I hope the explanation can be understood, and I would welcome anyone else's 
wording of what I've tried to say above ... all I know is that I learned 
the pattern from somewhere and I understand what it is doing and it has 
worked for me ... but I have to wave my arms to convey my thoughts on this.</para>


      </answer>
    </qandaentry>


   


    <qandaentry>
      <question>
	<para>SVG Image to fill full screen</para>
      </question>
      <answer>
	<para role="author">Jirka Kosek</para>
	<literallayout>
> You are correct, this 'half leading' resulted in a small white line above my SVG. 
> With line-height="1.0" this line vanished and I could make the SVG image 1mm higher.
> But, there is still a tine white line below the SVG, 
> it is still not possible to make it as high as the page.
>
> I just want the SVG to be rendered even if it has 
> the same size (or is bigger) than the page! Is this possible with XEP?
</literallayout>

<para>You can put your image into block-container and then use absolute positioning to fine tune placement of image, e.g.:</para>

<literallayout>
&lt;fo:block-container absolute-position="absolute"
                  left="-1.5cm"
                  top="-1cm" >
  ...
&lt;/fo:block-container> 
	</literallayout>

      </answer>
    </qandaentry>



    <qandaentry>
      <question>
	<para>Image scaling</para>
      </question>
      <answer>
	<para role="author">Angela Williams</para>
	<literallayout>

>I have tried playing with the margins and set them to
>0.5in for both the page and region-body. Then I set
>the content-height and content-width values to
>"scale-to-fit".

>This  says "no space for element to recover".
</literallayout>
<para>
 Your image is 212.09mm x 190.5mm but the available
space inside your region-body is only 200.4mm x 189.9mm.=20</para>
<para>
page_height - page_top - page_bottom - body_top - body_bottom
max-height of region-body content area. (279.4mm (11in) - 5mm - 5mm -
39mm - 30mm
=200.4mm</para>
<para>
page_width - page_left - page_right - body_left - body_right =
max-width
of region-body content area (215.9mm (8.5in) - 5mm - 5mm - 8mm - 8mm
= 189.9mm)</para>
<programlisting>
&lt;fo:external-graphic content-height="scale-to-fit" height="200.4mm"
content-width="scale-to-fit" width=3D"189.9mm" src="url({@href})"/> =
works.</programlisting>

<para>I have found http://www.onlineconversion.com/length_common.htm useful to
convert between mm and inches to determine dimensions - I recommend
using the same standard of measurement.</para>

      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>Wrapping text round an image</para>
      </question>
      <answer>
	<para role="author">Ken Holman</para>
	<literallayout>

>I have an image, and I want to have text to flow
>around it like this pattern:
>
>   text1   image___
>   text1   |       |
>   text2   |       |
>   text2   |_______|
>text2 text2 text2....
>
>my code is:
>
>&lt;fo:float float="left">
>  &lt;fo:block>text1  text1
>    &lt;fo:external-graphic src="url(blomma.jpg)"/>
>text2 text2 text2 text2.....
>  &lt;/fo:block>
>&lt;/fo:float>
>but it seems that it doesn't work as I expected.
</literallayout>


<para>You are looking at it backwards.  You should be floating the image so
that the text flow wraps around the floated image.</para>
<programlisting>
>Can somebody help me please?
>Does xep support float and floats attributes?</programlisting>
<para>
Indeed it does.  Consider the example below.  The float is marked to
go at the end of the line (using start/end is better than using
left/right for internationalized stylesheets), the text is then just
part of the regular flow and flows around the floated image.</para>

	<programlisting>

&lt;?xml version="1.0" encoding="US-ASCII"?>
&lt;root xmlns="http://www.w3.org/1999/XSL/Format"
      font-family="Times" font-size="20pt">

  &lt;layout-master-set>
    &lt;simple-page-master master-name="frame"
                        page-height="297mm" page-width="210mm"
                        margin-top="15mm" margin-bottom="15mm"
                        margin-left="15mm" margin-right="15mm">
      &lt;region-body region-name="frame-body"/>
    &lt;/simple-page-master>
  &lt;/layout-master-set>

  &lt;page-sequence master-reference="frame">
    &lt;flow flow-name="frame-body">
      &lt;float float="end">
        &lt;block>
          &lt;external-graphic alignment-baseline="after-edge"
                            width="5cm" content-width="5cm"
                            src='url("AnImage.jpg")'/>
        &lt;/block>
      &lt;/float>
      &lt;block>
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi est
eros, auctor quis, rutrum a, bibendum vitae, enim. Suspendisse dolor
elit, dapibus non, tincidunt quis, sodales vitae, magna. Maecenas
odio ipsum, bibendum id, feugiat luctus, aliquet sed, mauris.
Suspendisse ut sem suscipit lectus volutpat nonummy. Nam at ipsum ut
est luctus scelerisque. Duis id nulla eu pede interdum elementum.
Suspendisse lacinia, ante ut lobortis rhoncus, dolor purus tempor
pede, ut accumsan risus ante eu tellus. Mauris in nisl a ligula
fringilla pretium. Nunc et lacus in orci fringilla sodales. Aenean
feugiat justo. In hac habitasse platea dictumst. Nam ac urna sit amet
augue facilisis sodales. Integer lacinia vulputate wisi. Nullam
posuere magna eu ante. Integer in nisl. Sed enim tellus, nonummy
vitae, scelerisque a, viverra id, tortor.
      &lt;/block>
    &lt;/flow>
  &lt;/page-sequence>
&lt;/root>
</programlisting>
      </answer>
    </qandaentry>



    <qandaentry>
      <question>
	<para>Align graphic with top of text</para>
      </question>
      <answer>
	<para role="author">Ken Holman</para>
	<literallayout>

>What I want for inline image alignment is this, :
>
>  +-+  Blahhhh, Blahhh, Blahhh, Blahhh, Blahhh,
>   |  |
>  + +
</literallayout>
<para>
That is an incorrect default ... the default is that the base of the
image sits on top of the text baseline.</para>

	<literallayout>>In XEP, I get this:
>
>  +-+
>   | |
>  +-+  Blahhhh, Blahhh, Blahhh, Blahhh, Blahhh,</literallayout>

<para>Yep!</para>

	<literallayout>>
Basically, I want the image to output from the top down, aligned
>with the top of the text with XEP. Is this possible?</literallayout>

<para>Absolutely it is:  in XSL-FO the alignment-adjust= specifies which
point of the object is to be used for alignment, while the
alignment-baseline= specifies the point of the line to which the
object is aligned.</para>
<para>
So you want to align the before-edge of your image with the
before-edge of the line.  Seems to work fine in XEP.</para>

<para>I hope the example below helps.  Just plug in the name of your image
in place of "star.gif".</para>


	<programlisting>
&lt;?xml version="1.0" encoding="US-ASCII"?>&lt;!--align2.fo-->
&lt;root xmlns="http://www.w3.org/1999/XSL/Format"
      font-family="Times" font-size="20pt">

  &lt;layout-master-set>
    &lt;simple-page-master master-name="frame"
                        page-height="297mm" page-width="210mm"
                        margin-top="15mm" margin-bottom="15mm"
                        margin-left="15mm" margin-right="15mm">
      &lt;region-body region-name="frame-body"/>
    &lt;/simple-page-master>
  &lt;/layout-master-set>

  &lt;page-sequence master-reference="frame">
    &lt;flow flow-name="frame-body" xmlns="http://www.w3.org/1999/XSL/Format">
      &lt;block>
        This is a test of image alignment; "Xy" is used
        to show the location of the top, baseline, and
        bottom of the character box.&lt;/block>
      &lt;block>
        Default: image sits on baseline
        Xy&lt;external-graphic src='url("star.gif")'/>yX
      &lt;/block>
      &lt;block>
        Aligning the base to the bottom of the line:
        Xy&lt;external-graphic src='url("star.gif")'
                            alignment-baseline="after-edge"/>yX
      &lt;/block>
      &lt;block>
        Aligning the top to the top of the line:
        Xy&lt;external-graphic src='url("star.gif")'
                            alignment-adjust="before-edge"
                            alignment-baseline="before-edge"/>yX
      &lt;/block>
      &lt;block>End of test&lt;/block>
    &lt;/flow>
  &lt;/page-sequence>
&lt;/root>
</programlisting>
      </answer>
    </qandaentry>

  </qandaset>
</webpage>


<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
