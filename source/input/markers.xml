<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2003-12-15 15:26:11 dpawson"   -->
<!DOCTYPE webpage  SYSTEM "../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="markers">
<head>
<title>Markers</title>
<summary>Markers</summary>
  <keywords>docbook faq FAQ</keywords>
</head>
 
<qandaset>
 





    <qandaentry>
      <question>
	<para>Markers</para>
      </question>
      <answer>
	<para role="author">Wendell Piez</para>
	<literallayout>

question is how would I use XSL to "modify" or "repopulate" markers
>whenever the pages "hit" (or are incident upon) content from the "next"
>chapter.
</literallayout>
<para>If a new chapter starts and a new marker is presented to the formatter, it 
knows how to do the modification on the pages. (That's what markers are
for.)</para>
<para>
Your solution is as easy as:</para>
	<programlisting>
&lt;xsl:template match="chapter">
   &lt;fo:block>
     &lt;fo:marker marker-class-name="chapter-title">
        &lt;xsl:apply-templates select="title" mode="header"/>
     &lt;/fo:marker>
     &lt;xsl:apply-templates/>
   &lt;/fo:block>
&lt;xsl:template>
</programlisting>
<para>Each chapter gets a marker with its own title in it; the fo:retrieve-marker 
(which refers to this marker class), placed into the static content (that 
is, the header), then updates itself. That job is done by the formatter; as 
long as the marker is there, your XSLT doesn't have to do anything else.</para>

      </answer>
    </qandaentry>




    <qandaentry>
      <question>
	<para>Static content dependant on page content</para>
      </question>
      <answer>
	<para role="author">Mike Grimley</para>
	<literallayout>
> Actually every following block-container will eclipse previous one in
> the order of retrieve-markers. So I suppose you have first
> retrieve-marker with class 'unchanged' and second retrieve-marker with
> class 'changed' - in this case the latter one will mask the former
> one. I believe that in such a way you can achieve required
> functionality without any z-indexes with any number of classes.
</literallayout>
<para>
So, what's the code?</para>

<para>
Because I simply have a choice of two, I have the block displaying 'Unchanged' by default. I only set the marker if the status of an instruction is 'Changed'. If there is a 'Changed' marker, it simply covers the default.</para>
	<programlisting>
&lt;fo:static-content flow-name="EvenPage-header">
  &lt;fo:block-container height="30pt" 
	  position="fixed" top="2.5pc" text-align="center">
   &lt;fo:block xsl:use-attribute-sets="Status">Unchanged&lt;/fo:block>
	&lt;/fo:block-container>
	&lt;fo:block-container height="30pt" 
          position="fixed" top="2.5pc" text-align="center">
	 &lt;fo:retrieve-marker retrieve-class-name="Status" 
         retrieve-position="first-including-carryover" 
         retrieve-boundary="page"/>
      &lt;/fo:block-container>
&lt;/fo:static-content>
</programlisting>

<para>This offers a defined alternative:</para>

<para>
Markers:</para>
	<programlisting>
  &lt;fo:marker marker-class-name="unchanged">
    &lt;fo:block-container position="absolute" 
         top="0.5in" left="1in" width="2in" height="0.5in" 
         background-color="white">
      &lt;fo:block>**unchanged**&lt;/fo:block>
    &lt;/fo:block-container>
  &lt;/fo:marker>
  
  or
  
  &lt;fo:marker marker-class-name="changed">
    &lt;fo:block-container position="absolute" top="0.5in" 
          left="1in" width="2in" height="0.5in" 
           background-color="white">
      &lt;fo:block>++changed++&lt;/fo:block>
    &lt;/fo:block-container>
  &lt;/fo:marker>
</programlisting>
<para>and Static content:</para>
	<programlisting>
  &lt;fo:static-content flow-name="xsl-region-before">
      &lt;fo:retrieve-marker retrieve-class-name="unchanged" 
                retrieve-position="first-including-carryover" 
                retrieve-boundary="page"/>
      &lt;fo:retrieve-marker retrieve-class-name="changed" 
             retrieve-position="first-including-carryover" 
             retrieve-boundary="page"/>
  &lt;/fo:static-content>
</programlisting>

	<literallayout>
> Does one have to have fo:retrieve-markers in the order of 
>  accending precedence?
</literallayout>
<para>Yes.</para>

	<literallayout>> Then if there is a marker to retrieve the latest retrieved will generate an
> absolute container on top of the rest?
</literallayout>
<para>Yes.</para>


      </answer>
    </qandaentry>

   


    <qandaentry>
      <question>
	<para>Section X continued.</para>
      </question>
      <answer>
	<para role="author">Ken Holman</para>
	<literallayout>
> I'm using a retrieve-marker in an xsl-region-before in the hope of
> showing a 'section X continued' label when page breaks occur within
> certain blocks or block-containers. Since there's no such thing as a
> "first-within-carryover" value for the retrieve-position attribute,
> is there any way to do this without sometimes grabbing the first
> marker in the current page?
	</literallayout>




 <para>It sounds like you are not "undoing" the marker at the beginning
 and end of your sensitive blocks or containers.</para>

	<literallayout>> As it is, I'm using "first-including-carryover" so if the page break
> occurs between blocks, there is no carryover and the first marker in
> the current page displays. As it should, per the spec, but not per my
> intent. Does anyone know of a workaround?</literallayout>

<para> Yes, but doing it for a header is not as straightforward as doing
 it for a footer.</para>

 <para>Here is an example where I'm putting "continued..." at the bottom of
 a page in a footer:</para>

 <programlisting>  &lt;block>
     &lt;marker marker-class-name="section">Section One - 1.&lt;/marker>
     &lt;marker marker-class-name="continued">(continued...)&lt;/marker>
     &lt;block>1. Section One&lt;/block>
     &lt;block space-before="1em">This is a test&lt;/block>
     ...
     &lt;block space-before="1em">This is a test&lt;/block>
   &lt;/block>
   &lt;block>
     &lt;marker marker-class-name="continued">&lt;/marker>
   &lt;/block></programlisting>

<programlisting>   &lt;block space-before="2em">
     &lt;marker marker-class-name="section">Section Two - 2.&lt;/marker>
     &lt;marker marker-class-name="continued">(continued...)&lt;/marker>
     &lt;block>2. Section Two&lt;/block>
     &lt;block space-before="1em">This is a test&lt;/block>
     ...
     &lt;block space-before="1em">This is a test&lt;/block>
   &lt;/block>
   &lt;block>
     &lt;marker marker-class-name="continued">&lt;/marker>
   &lt;/block></programlisting>
<para>
 In fact this undoing of the marker would be important for you
 regardless, because what if the block you have ends right at the
 very bottom of the page?  There is always room for an empty block
 at the bottom of the page.  If your table ends right at the bottom,
 then you don't want the marker defined for the bottom of that page
 or the top of the next.</para>
<para>
 BTW, the kind of retrieval you need for a footer is the last within
 the page:</para>
<programlisting>
   &lt;static-content flow-name="frame-after">
     &lt;block text-align="end" font-style="italic" font-size="12pt">
       &lt;retrieve-marker retrieve-class-name="continued"
                   retrieve-position="last-starting-within-page"/>
     &lt;/block>
   &lt;/static-content></programlisting>
<para>
 But what you need is a marker retrieved into the header and that is
 more subtle.  First, you have to be prepared for a block starting
 at the very top of the page (where you do not want it to show),
 which means you have to both make sure it is not defined (that is,
 defined as empty) as the first block on the page, and then define
 it as part of the block.  *Then* you have to undefine it at the end
 again so that the definition isn't lying around to be retrieved on
 the next page.</para>
<para>
 Here is code that works for "...continued" in the header:</para>

<programlisting>   &lt;static-content flow-name="frame-before">
     &lt;block text-align="end" font-style="italic" font-size="12pt">
       &lt;retrieve-marker retrieve-class-name="continued"
                   retrieve-position="first-including-carryover"/>
     &lt;/block>
   &lt;/static-content>
   ...
   &lt;block keep-with-next="always">
     &lt;marker marker-class-name="continued">&lt;/marker>
   &lt;/block>
   &lt;block>
     &lt;marker marker-class-name="section">Section One - 1.&lt;/marker>
     &lt;marker marker-class-name="continued">(...continued)&lt;/marker>
     &lt;block>1. Section One&lt;/block>
     &lt;block space-before="1em">This is a test&lt;/block>
     &lt;block space-before="1em">This is a test&lt;/block>
     ...
     &lt;block space-before="1em">This is a test&lt;/block>
     &lt;block space-before="1em">This is a test&lt;/block>
   &lt;/block>
   &lt;block keep-with-previous="always">
     &lt;marker marker-class-name="continued">&lt;/marker>
   &lt;/block></programlisting>

<para> Note how my use of keeps will ensure the empty definition is on the
 same page and before the non-empty definition, thus ensuring the
 first one on the page is the empty definition.</para>
<para>
 Since you are pulling into the header, your use of "first including
 carryover" is appropriate since the marker would have been undone
 if the table ended on the page before.</para>
	
      </answer>
    </qandaentry>




 </qandaset>
</webpage>


<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->