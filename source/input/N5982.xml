<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2002-10-27 08:17:31 Dave Pawson"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="N5982">
<head>
<title>Parameters</title>
<summary>Parameters</summary>

     <keywords>xsl parameters, default parameters</keywords>
      <description>default parameter values</description>

</head>
<qandaset>

<qandaentry>
      <question>
	<para>Default value for parameters</para>
      </question>
      <answer>
	<para role="author">David Carlisle</para>
	<literallayout>
>I can't figure out what
>  &lt;xsl:param name="data" select="self::node()[false()]"/>
>  could be doing; is supposed to do; does it make any sense?
	</literallayout>

<para>It is declaring the parameter and giving it a default value of an empty
node set. Yes it does make sense, I usually use "/.." for that but 
the above would do.</para>
<para>
If you expect a param to be passed in as a node set then if you leave
the default being 
 &lt;xsl:param name="data" />
it will be an empty string and things like
select="$param/a/b"
would be a type error.</para>
<para>
However if you make the default an empty node set then these expressions
are legal but select nothing, which may be what is required.</para>

      </answer>
    </qandaentry>


 <qandaentry>
   <question>
    <para>Accessing parameters which contain markup</para>

   </question>
   <answer>
    <para role="author">Kay Michael</para>
    <programlisting format="linespecific">

> I have the following template rule
> 
> &lt;xsl:template name="separated-list">
> 	&lt;xsl:param name="nodes"/>
> 	&lt;xsl:param name="separator"/>
> 	&lt;xsl:for-each select="$nodes">
> 		&lt;xsl:value-of select="."/>
> 		&lt;xsl:if test="position() != last()">
> 			&lt;xsl:value-of select="$separator"/>
> 		&lt;/xsl:if>
> 	&lt;/xsl:for-each>
> &lt;/xsl:template>
> 
> Sometimes separator is "," and other times it's a &lt;BR>
	</programlisting>

<para>If the separator is a node-set containing a single &lt;BR/> element, then
&lt;xsl:value-of> will output nothing (the string value of an empty element).
Try using &lt;xsl:copy-of> instead.</para>

   </answer>
  </qandaentry>

<qandaentry>

        <question>
<para>How to output the current users login and full name</para>
</question>
    
        <answer>
<para role="author">Norm Walsh</para>


       

    <para>A possibility is to construct a little wrapper XSL stylesheet that
defines these as variables. Instead of processing the documents with
stylesheet x.xsl, construct y.xsl:
</para>
    <programlisting>&lt;?xml version='1.0'?&gt;
&lt;xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Tranform"     
	version="1.0"&gt;

&lt;xsl:variable name="login-name"&gt;ndw&lt;/xsl:variable&gt;
&lt;xsl:variable name="full-name"&gt;Norman Walsh&lt;/xsl:variable&gt;

&lt;xsl:include href="x.xsl"/&gt;
&lt;/xsl:stylesheet&gt;
</programlisting>
    <para>And refer to the variables login-name and full-name in your x.xsl
stylesheet.
</para>      

        </answer>

    </qandaentry>
<qandaentry>
    <question>
<para>Passing top level param</para>
</question>
    
    
      
    
    <answer>
<para role="author">Eric van der Vlist</para>
      <programlisting>

&gt; 
&gt; The XSL Transformation v1.0 spec says about passing top level params:
&gt; "XSLT does not define the mechanism by which parameters are passed to the
&gt; stylesheet"
&gt; 
&gt; Does anyone now if any xslt processors does this?</programlisting>


     <para>XT does...

      Quoting <ulink url="http://jclark.com/xml/xt.html">James C</ulink> :


     <quote>java -Dcom.jclark.xsl.sax.parser=your-sax-driver
com.jclark.xsl.sax.Driver source stylesheet result name=value...

The name=value arguments are optional and specify parameter
names and values; they can occur in any order with respect
to the other arguments.  They will be ignored unless the
stylesheet contains a corresponding top-level xsl:param
element. The value of the parameter will be of type string.
</quote>
</para>

    <para>Michel CASABIANCA gives an example
</para>
    <programlisting>&lt;?xml version="1.0" encoding="iso-8859-1"?&gt;
&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"&gt;

  &lt;xsl:output method="text"/&gt;
  &lt;xsl:param name="param"&gt;default&lt;/xsl:param&gt;

  &lt;xsl:template match="/"&gt;
    &lt;xsl:call-template name="test"/&gt;
  &lt;/xsl:template&gt;

  &lt;xsl:template name="test"&gt;
    &lt;xsl:value-of select="$param"/&gt;
  &lt;/xsl:template&gt;
&lt;/xsl:stylesheet&gt;
</programlisting>
    <para>called with any xml file like this :

     <computeroutput>java com.jclark.xsl.sax.Driver test.xml test.xsl param=value</computeroutput>

Will output "value" rather than default.
    </para>


    </answer>
  </qandaentry>
<qandaentry>
 <question>
   <para>Using stylesheet parameters</para>
 </question>
 
<answer>
    <para role="author">Jeni Tennison</para>
    <para>

To use parameters passed in from an asp page, you have to:
1. pass the parameters from the asp page
2. accept the parameters within the stylesheet
3. use the parameters within the stylesheet</para>

    <para>I'm not sure which of these stages you're having problems with.  Ben assumed
you were having problems with stage 1, which is passing the parameters into
the stylesheet from the asp page.  When he talked about the parser you were
using, he meant what are you using to make the stylesheet run, to call the
stylesheet from the asp page.  It sounds as though it's MSXML, which is
Microsoft's XML/XSLT processor - you should make sure you have the most
recent version from their web site.  Ben offered to help you more with the
asp side of things, and I don't know much about it, so I'll concentrate on
giving you a hand with the other two stages.
</para>
    <para>For Stage 2, accepting the parameters within the stylesheet, you need to
declare them using xsl:param elements at the top level of the stylesheet
(immediate children of the xsl:stylesheet element):
</para>
    <programlisting>&lt;xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  ...
  &lt;xsl:param name="foo" />
  ...
&lt;/xsl:stylesheet>
</programlisting>
    <para>You can specify a default value for the parameter either as its content or
as the value of the 'select' attribute, if you want.  Usually stylesheet
parameter values are strings, so be aware, if you use the 'select'
attribute, that you have to put quotes around the default value, otherwise
it will interpret it as an XPath, so use:
</para>
    <programlisting>  &lt;xsl:param name="foo" select="'bar'" /></programlisting>

    <para>rather than:
</para>
    <programlisting>  &lt;xsl:param name="foo" select="bar" /></programlisting>

    <para>which will try to set it to the value of any 'bar' elements that are lying
around.
</para>
    <para>As far as using the parameter is concerned (Stage 3), you can refer to a
particular parameter using '$parameter-name', so '$foo' in the case above.
If you want to test its value to see what to generate, for example, you can
use:
</para>
    <programlisting>  &lt;xsl:if test="$foo = 'bar'">
    &lt;!-- some content -->
  &lt;/xsl:if>
</programlisting>

   </answer>
 
  </qandaentry>



  <qandaentry>
   <question>
    <para>Can I pass a set of nodes as a parameter to a template</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
 
<para>
Yes, but "node sets" in XSLT terminology are sets of nodes from the source
document, not sets of nodes that you generate within the stylesheet.  You
can make a parameter or variable take a node set as a value by setting that
value with the 'select' attribute.
    </para>
<para>When you set a parameter or variable value using the content of the
parameter or variable, then you create a "result tree fragment", which is
exactly what it says on the tin - a fragment of the result tree.  Result
tree fragments aren't the same as source nodes, and you can do only limited
things with them.  One of the things that you *can't* do is process them in
any way.
    </para>
    <para>Some processors have extension functions that allow you to convert result
tree fragments into node sets. 
xt:node-set()
saxon:node-set()
xalan:node-set()</para>

<para>Without using an extension function, you may have to do two passes: one
pass creating a result that you then use as the input to the next pass.
</para>
   </answer>
  </qandaentry>

</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
