<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2003-03-30 13:29:35 dpawson"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="N6191">

<head>
<title>Selection</title>
<summary>Selection</summary>
    <keywords>xslt selecting </keywords>
      <description>Selecting and matching nodes, xslt</description>
</head>
<qandaset>
<qandaentry>
    
        <question>
<para>Select and match</para>
</question>
    
    
        <answer>
<para role="author">Mike Brown</para>

      
            

    <para>Certain instructions have a select attribute, others have a
match attribute, but never both. You "select" a set of nodes
that you want to process. You declare that a template
"matches" nodes that have a certain pattern.
</para>
    <para>An XSLT processor starts at the root node and finds the
template that best matches that node. It executes the
instructions in that template. Then it is done. An
apply-templates instruction in that one template tells the
processor to select another set of nodes and, for each of
those nodes, find the template that best matches it and
execute the instructions in it. Many templates may match a
given node; there are rules in the spec for determining
which template will be chosen.
</para>
    <para>Templates are not executed in a specific order. Templates
only say "if the XSLT processor decides that this
template provides the best match for the node *that it is
already looking at*, here are some instructions to
execute." They are like subroutines, whereas the main
program is hidden inside the XSLT processor.
</para>
            
    
        </answer>
  
    </qandaentry>



<qandaentry>
 <question>
   <para>Using parameter with xsl:for-each</para>
 </question>
 
<answer>
    <para role="author">David Carlisle</para>
    <para>

you can't in standard xsl evaluate a string as an xpath
expresion.</para>


    <programlisting>you have &lt;xsl:value-of select="$selectpath"/></programlisting>

    <para>but you passed it a string: "Resources/Resource"

so it is equivalent to

     <computeroutput>&lt;xsl:value-of select="'Resources/Resource'"/></computeroutput>
</para>

    <para>Mike Brown expands:
To restate what David Carlisle was saying, if an expression that
identifies a node-set is expected, the only kind of variable you can use
in the expression is one that is itself a node-set. You can't use a string
that resembles an expression that, if evaluated as an expression, would
return a node-set.
</para>
    <para>So you can never define a string like 'foo/bar' and then put that in an
expression like /path/to/$yourstring/and/beyond. You can, however, use
non-node-set variables in predicates. So if you had, say, variable $x that
was the string 'foo', /path/to/*[name()=$x] wouldn't be out of the
question.
</para>


   </answer>
  </qandaentry>

  <qandaentry>
   <question>
    <para>Invalid predicate</para>

   </question>
   <answer>
    <para role="author">Gary L Peskin</para>
    <para>

Why is it that a predicate is not allowed when I do something like</para>

     <programlisting>&lt;xsl:variable name="foo" select=".[@id=1]"/></programlisting>

<para>I mean if the current node has an 'id' attribute numerically equal to the
number 1, then $foo will be the current node; otherwise it will be an
empty node-set, right? Both SAXON and XT gripe at me for having a
predicate after . at all. I can't even say ".[true()]"! What's the deal?
I don't see anything in XPath prohibiting this.</para>


    <programlisting>Try self::node()[@id=1]</programlisting>

<para>The "." is an AbbreviatedStep (production [12]) and cannot be followed
by a predicate according to production [4].  That production requires
</para>
    <programlisting>	AxisSpecifier NodeTest Predicate*</programlisting>

<para> so "." is not just self::node(), it is an abbreviation of the
*step* self::node() -- axis, node test, and any predicates inclusive.
Makes sense, then, but isn't obvious by the prose portion of the XPath
spec.
</para>
<para>Mike Kay adds:
</para>
<para>"." is an abbreviated step, not an abbreviated the-part-of-the-step-before
the-predicates. Why that should be, I don't know, but that's what XPath
says.
</para>

   </answer>
  </qandaentry>


  <qandaentry>
   <question>
    <para>footnotes</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
    <programlisting format="linespecific">

>What I need to do is to process all FN elements located prior to the current
>&lt;P> element, BUT after the last set of &lt;FN> elements I already processed!
    </programlisting>
<para>I guess that your XML looks something like:
    </para>
    <programlisting>&lt;doc>
  &lt;FN id="fn1">
    &lt;P>Footnote 1&lt;/P>
  &lt;/FN>
  &lt;P id="para1">Paragraph 1&lt;/P>
  &lt;section>
    &lt;FN id="fn2">
      &lt;P>Footnote 2&lt;/P>
    &lt;/FN>
    &lt;H1>Heading&lt;/H1>
    &lt;P id="para2">Paragraph 2&lt;/P>
  &lt;/section>
&lt;/doc>
</programlisting>

<para>In other words:
(a) FNs are always associated with (and processed alongside) Ps, not with
any other elements
(b) FNs may not immediately precede their respective P
    </para>
<para>If this is the case, given that your current node is a P element, you can
get the FN elements between it and the previous P using:
    </para>
    <programlisting>preceding::FN[generate-id(following::P[1]) = 
                         generate-id(current())]</programlisting>

    <programlisting>In other words:
any FN element that precedes the current element in the source document 
  such that:
    the unique ID for the immediately following P of the FN is the same as
      the unique ID of the current (P) element</programlisting>

<para>[You don't have to generate-id()s if you have some other natural identifier
for the P elements, like an 'id' attribute.]
    </para>
<para>So, you could probably use something like:
    </para>
    <programlisting>&lt;xsl:template match="P">
  &lt;xsl:apply-templates />
  &lt;xsl:apply-templates 
     select="preceding::FN[generate-id(following::P[1]) = 
                                             generate-id(current())]" />
&lt;/xsl:template>

&lt;xsl:template match="FN">
  &lt;p />&lt;xsl:value-of select="P" />
&lt;/xsl:template>
</programlisting>
<para>The reason I've done away with the 'Dump Footnotes' named template was that
it's not really necessary: you can just select the FNs that you want to
process within the 'P'-matching template as above rather than passing them
as a parameter to a template.  Of course you can change it back if you want
to :)
    </para>

   </answer>
  </qandaentry>
 <qandaentry>
   <question>
    <para>Unknown element selection (parent is known)</para>

   </question>
   <answer>
    <para role="author">Oliver Becker</para>
    <programlisting format="linespecific">

> How do I select a record without knowing the
> name of it. What I know is the name of the parent,
> 
> &lt;PARENT>
>     &lt;???>Donald Ball&lt;/???>
>     &lt;???>23&lt;/???>
> &lt;/PARENT>
	</programlisting>

<para>PARENT/*
selects all children of the PARENT element</para>
<para>
PARENT/*[1]
selects only the first child</para>
<programlisting format="linespecific">
> Another thing i do not know is the number of elements. </programlisting>
<para>
count(PARENT/*)</para>

   </answer>
  </qandaentry>

 
   <qandaentry>
   <question>
    <para>Selecting only specific elements</para>

   </question>
   <answer>
    <para role="author">Miloslav Nic</para>
    <para>

Source
	</para>
	<programlisting>&lt;a>
 &lt;b>some text&lt;/b>
 &lt;b>some more text&lt;/b>
 &lt;b>some other text&lt;/b>
 &lt;b>some last text&lt;/b>
&lt;c>some c text&lt;/c>
&lt;/a>

Wanted output

&lt;a>
 &lt;bset>
  &lt;b>some text&lt;/b>
  &lt;b>some more text&lt;/b>
  &lt;b>some other text&lt;/b>
  &lt;b>some last text&lt;/b>
 &lt;/bset>
&lt;c>some c text&lt;/c>
&lt;/a>
</programlisting>
<para>
Try
	</para>
	<programlisting>
&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
version="1.0">

&lt;xsl:template match="a">
  &lt;a>
    &lt;xsl:apply-templates select="*"/>
  &lt;/a>
&lt;/xsl:template>

&lt;xsl:template match="b">
    &lt;xsl:if test="not(name(preceding-sibling::*[1])='b')">
      &lt;bset>
	&lt;xsl:copy-of select="."/>
	&lt;xsl:apply-templates select="following-sibling::*[1]" mode="set"/>
      &lt;/bset>      
    &lt;/xsl:if>
&lt;/xsl:template>

&lt;xsl:template match="b" mode="set">
  &lt;xsl:copy-of select="."/>
  &lt;xsl:apply-templates select="following-sibling::*[1]" mode="set"/>
&lt;/xsl:template>

&lt;xsl:template match="*" mode="set"/>

&lt;xsl:template match="*">
  &lt;xsl:copy-of select="."/>
&lt;/xsl:template>

&lt;/xsl:stylesheet>
</programlisting>

<para>It needs some more work to make it really general, but I hope it
will give you some ideas.
	</para>

   </answer>
  </qandaentry>

   <qandaentry>
   <question>
    <para>Determining if the first child is a specific element</para>

   </question>
   <answer>
    <para role="author">David Carlisle</para>
    <programlisting format="linespecific">

> I need to know if the first child of a node is an
> element (para, in this case).  The test needs to be done in the parent's
> &lt;template> element, prior to an &lt;apply-templates/>.  Is there a
> straightforward &lt;if> test I can use for this case?
	</programlisting>


	<programlisting>&lt;xsl:if test="*[1][self::para]">David&lt;/xsl:if></programlisting>


<para>Jeni Tennison expands:
	</para>
<para>
It's not clear whether you want to know whether it's a specific
element (a para element) or an element rather than some other type of
node (e.g. text or a comment).
	</para>
	<programlisting>You can get the first child node using the XPath:

  child::node()[1]

You can get the first child element using the XPath:

  child:*[1]

You can test whether something is an element using the test:

  self::*

You can test whether something is a para element using the test:

  self::para

So, you can find the first child node that is an element using the
XPath:

  child::node()[1][self::*]
</programlisting>  
  


<para>If the first child node is not an element, then no nodes will be
selected with this XPath.  Within an xsl:if test, a node set with no
nodes in it evaluates as false() while one that does hold nodes
evaluates as true().  So, to test whether the first child node of
the current node is an element, use:
	</para>
	<programlisting>  &lt;xsl:if test="child::node()[1][self::*]">
     ...
  &lt;/xsl:if>
</programlisting>  
<para>  Mike Brown cautions here
	</para>  

<para>  Also be careful in this case that you have accounted for the possibility of 
  any whitespace-only text nodes. So, for example, if your document read:
	</para>  
	<programlisting>  &lt;CHAPTER>
     &lt;para>Here's my first child element, a para.&lt;/para>
  ...
  &lt;/CHAPTER>
</programlisting>  
<para>  Jeni's test above will fail on account of the whitespace-only text node 
  preceding the para element (it contains a carriage return and two space 
  characters).
	</para>  
	<programlisting>  Adjust for this by saying
  
  &lt;xsl:strip-space elements="CHAPTER"/>
  
  at the top level, or altering your test (somewhat horribly) to:
  
  &lt;xsl:if test="child::node()[not(.=normalize-space(.))][1][self::para]">
      ...
  &lt;/xsl:if>
</programlisting>  
<para>Jeni Continues:
	</para>
<para>To test whether the first child element of the current node is a para
element, use:
	</para>
	<programlisting>  &lt;xsl:if test="child::*[1][self::para]">
     ...
  &lt;/xsl:if>
</programlisting>  



<para>Wendell Piez wraps it up:
	</para>
<para>But of course after seeing Mike Kay's nice test in another thread, I 
realized a slightly more elegant and efficient version would be
	</para>
	<programlisting>&lt;xsl:if test="child::node()[normalize-space(.)][1][self::para]">
    ...
&lt;/xsl:if>
</programlisting>
<para>...which reveals further complications (funny how these things are). We 
have to make sure it's skipped only if it's a whitespace-only _text_ node 
(not, say, an empty element node). So....
	</para>
	<programlisting>&lt;xsl:if test="child::node()[not(self::text()) or 
normalize-space(.)][1][self::para]">
    ...
&lt;/xsl:if>
</programlisting>
<para>The first predicate evaluates to true if the child node is not a text node 
or if it has a non-whitespace string value. The second chooses the first of 
these. The third checks to see whether it's a para element.
	</para>
<para>For evident reasons, that &lt;xsl:strip-space .../> at the top level will be 
preferable, if you can use it.
	</para>

   </answer>
  </qandaentry>
 

 <qandaentry>
   <question>
    <para>Unique element values only</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
    <programlisting format="linespecific">

> I've tried manipulating the order of the ACHFee element with
> &lt;xsl:sort select="feeLevelBottom"/> and so-on. I've used every
> combination of preceding::, following::, following-sibling:: that I
> can come up with, but I still cannot make ONLY UNIQUE cell contents
> be displayed</programlisting>
<para>
Here's what you're missing: axes *always* operate on the structure of
the original XML, it don't matter how much you sort or what axes you
try.</para>
<para>
At the moment you trying to do (roughly)</para>
	<programlisting>
  for each ACHfee
    sorted according to feeLevelBottom
       if you haven't already processed something with the same
             feeLevelBottom
          then process this one
</programlisting>
<para>Instead, you need to turn it around so that you only select the first
ACHfees with a particular feeLevelBottom in the first place, iterate
over them, and sort *them* according to the feeLevelBottom.</para>
<para>
For example:</para>
	<programlisting>
&lt;xsl:for-each select="//ACHFee
                       [not(feeLevelBottom =
                            preceding::ACHFee/feeLevelBottom)]">
   &lt;xsl:sort select="feeLevelBottom" />
   ...
&lt;/xsl:for-each>
</programlisting>                       
<para>You may find it more efficient to use the Muenchian Method to get the
list of fees with unique feeLevelBottoms.  Define a key:</para>
	<programlisting>
&lt;xsl:key name="fees"
         match="ACHFee"
         use="number(feeLevelBottom)" />
</programlisting>
<para>And then select the ACHFees with unique feeLevelBottom values with:</para>
	<programlisting>
&lt;xsl:for-each
   select="//ACHFee
             [count(.|key('fees', number(feeLevelBottom))[1]) = 1]">
   &lt;xsl:sort select="feeLevelBottom" />
   ...
&lt;/xsl:for-each>
</programlisting>

   </answer>
  </qandaentry>

 <qandaentry>
   <question>
    <para>XPath expression for every child but one</para>

   </question>
   <answer>
    <para role="author">Francis Norton and Jeni Tennison</para>
    <programlisting format="linespecific">

>  Is it possible to have an XPath expression that means "every child but one"
>  E.g. every child except &lt;title>
	</programlisting>
<para>
Yes, I think this came up fairly recently - the other solution is a bit
non-obvious:</para>
	<programlisting>
	&lt;xsl:apply-templates select="*[not(self::title)]"/>
</programlisting>

<para>Jeni adds</para>
<para>
Or, generally better because it avoids namespace problems:</para>
	<programlisting>
  &lt;xsl:apply-templates select="*[not(self::title)]" />
</programlisting>
   </answer>
  </qandaentry>
 <qandaentry>
   <question>
    <para>How to check is element has children</para>

   </question>
   <answer>
    <para role="author">Michael Kay</para>
    <para>

I need to test if an element has children 
	</para>
<para>Assuming it is the current element:</para>

	<programlisting>xsl:if test="node()" for any children, or

xsl:if test="*" for element children.
</programlisting>

   </answer>
  </qandaentry>

 <qandaentry>
   <question>
    <para>Pruning nodes not required</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison </para>
    <programlisting format="linespecific">

> I would like to be able to generate a stylesheet that will produce
> output documents that exclude all nodes that are not matched by any
> of the xpath expressions.
	</programlisting>
<para>The first thing is to work out a stylesheet that does what you want
with the expressions hard-coded into it...</para>
	<programlisting>
> From an algorithm point of view, one solution is to create a
> node-set collection of all nodes that should be copied to the output
> document. 
</programlisting>
<para>
OK, so first work out which nodes have been selected by the
expressions:</para>
	<programlisting>
&lt;xsl:variable name="selected-nodes"
              select="//product/@sku | //product/cost"/>
</programlisting>
<para>Then to work out which nodes should therefore be copied - that's all
those nodes that are selected, plus their ancestors:</para>

	<programlisting>&lt;xsl:variable name="copied-nodes"
              select="$selected-nodes | $selected-nodes/ancestor::*"/>
</programlisting>
<para>Now, starting from the top (the document element is always the first
in the list of copied nodes), we want to work through the document. If
the element is in the list of selected nodes, then we just want to
copy it completely. Otherwise, we want to copy the element, copy any
attributes that are in the list of selected nodes, and apply templates
to any of its children that are in the list of nodes to be copied.</para>
<para>
Now, working out whether a node is in a set of nodes involves a bit of
set manipulation - if you count how many nodes there are in the set
that's the union of the node and the node set, and it's the same as
the number of nodes in the node set, then the node has to be in the
node set.  So to work out whether the context node is in the set of
$selected-nodes, I can use:</para>
	<programlisting>
  count(.|$selected-nodes) = count($selected-nodes)
</programlisting>
<para>Since $selected-nodes and $copied-nodes are known up front, I'll
create variables that indicate how many nodes are in each of them:</para>

	<programlisting>&lt;xsl:variable name="nselected-nodes" select="count($selected-nodes)" />
&lt;xsl:variable name="ncopied-nodes" select="count($copied-nodes)"/>
</programlisting>
<para>So the template looks like:</para>
	<programlisting>
&lt;xsl:template match="*">
   &lt;xsl:choose>
      &lt;!-- this node is one of the selected nodes -->
      &lt;xsl:when test="count(.|$selected-nodes) = $nselected-nodes">
         &lt;xsl:copy-of select="."/>
      &lt;/xsl:when>
      &lt;xsl:otherwise>
         &lt;xsl:copy>
            &lt;!-- copy attributes that are selected nodes -->
            &lt;xsl:copy-of select="@*[count(.|$selected-nodes) =
                                    $nselected-nodes]"/>
            &lt;!-- apply templates to nodes that are to be copied -->
            &lt;xsl:apply-templates select="node()[count(.|$copied-nodes) =
                                                $ncopied-nodes]"/>
         &lt;/xsl:copy>
      &lt;/xsl:otherwise>
   &lt;/xsl:choose>
&lt;/xsl:template>
</programlisting>
<para>And there you have it.  That stylesheet will take the input you
specified and give the output you specified.</para>
<para>
Now all you have to do is create a stylesheet that creates it.  In
fact most of it is static, so you could bundle it out to a separate
stylesheet and include it rather than create it dynamically.  The only
thing that isn't static is the creation of the $selected-nodes
variable:</para>
	<programlisting>
&lt;xsl:variable name="selected-nodes"
              select="//product/@sku | //product/cost"/>
</programlisting>
<para>Assuming that you're using oxsl as the namespace prefix for your XSLT
namespace alias, then you need to create an oxsl:variable with a name
attribute equal to selected-nodes:</para>
	<programlisting>
&lt;oxsl:variable name="selected-nodes">
   ...
&lt;/oxsl:variable>
</programlisting>
<para>The select attribute is created dynamically by iterating over the
various expressions that you have (I'll assume their held in an $exprs
variable):</para>
	<programlisting>
&lt;oxsl:variable name="selected-nodes">
   &lt;xsl:attribute name="select">
      &lt;xsl:for-each select="$exprs">
         &lt;xsl:value-of select="." />
         &lt;xsl:if test="position() != last()"> | &lt;/xsl:if>
      &lt;/xsl:for-each>
   &lt;/xsl:attribute>
&lt;/oxsl:variable>
</programlisting>
<para>You should be able to create the rest of the stylesheet very
straight-forwardly.</para>

   </answer>
  </qandaentry>
<qandaentry>
   <question>
    <para>Pruning nodes not in xpath list</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
    <programlisting format="linespecific">

> I would like to be able to generate a stylesheet that will produce
> output documents that exclude all nodes that are not matched by any
> of the xpath expressions.</programlisting>

<para>The first thing is to work out a stylesheet that does what you want
with the expressions hard-coded into it...</para>

	<programlisting>> From an algorithm point of view, one solution is to create a
> node-set collection of all nodes that should be copied to the output
> document. 
</programlisting>

<para>OK, so first work out which nodes have been selected by the
expressions:</para>

	<programlisting>&lt;xsl:variable name="selected-nodes"
              select="//product/@sku | //product/cost"/>
</programlisting>
<para>Then to work out which nodes should therefore be copied - that's all
those nodes that are selected, plus their ancestors:</para>
	<programlisting>
&lt;xsl:variable name="copied-nodes"
              select="$selected-nodes | $selected-nodes/ancestor::*"/>
</programlisting>
<para>Now, starting from the top (the document element is always the first
in the list of copied nodes), we want to work through the document. If
the element is in the list of selected nodes, then we just want to
copy it completely. Otherwise, we want to copy the element, copy any
attributes that are in the list of selected nodes, and apply templates
to any of its children that are in the list of nodes to be copied.</para>
<para>
Now, working out whether a node is in a set of nodes involves a bit of
set manipulation - if you count how many nodes there are in the set
that's the union of the node and the node set, and it's the same as
the number of nodes in the node set, then the node has to be in the
node set.  So to work out whether the context node is in the set of
$selected-nodes, I can use:</para>

	<programlisting>  count(.|$selected-nodes) = count($selected-nodes)</programlisting>

<para>Since $selected-nodes and $copied-nodes are known up front, I'll
create variables that indicate how many nodes are in each of them:</para>

	<programlisting>&lt;xsl:variable name="nselected-nodes" select="count($selected-nodes)" />
&lt;xsl:variable name="ncopied-nodes" select="count($copied-nodes)"/>
</programlisting>
<para>So the template looks like:</para>
	<programlisting>
&lt;xsl:template match="*">
   &lt;xsl:choose>
      &lt;!-- this node is one of the selected nodes -->
      &lt;xsl:when test="count(.|$selected-nodes) = $nselected-nodes">
         &lt;xsl:copy-of select="."/>
      &lt;/xsl:when>
      &lt;xsl:otherwise>
         &lt;xsl:copy>
            &lt;!-- copy attributes that are selected nodes -->
            &lt;xsl:copy-of select="@*[count(.|$selected-nodes) =
                                    $nselected-nodes]"/>
            &lt;!-- apply templates to nodes that are to be copied -->
            &lt;xsl:apply-templates select="node()[count(.|$copied-nodes) =
                                                $ncopied-nodes]"/>
         &lt;/xsl:copy>
      &lt;/xsl:otherwise>
   &lt;/xsl:choose>
&lt;/xsl:template>
</programlisting>
<para>And there you have it.  That stylesheet will take the input you
specified and give the output you specified.</para>
<para>
Now all you have to do is create a stylesheet that creates it.  In
fact most of it is static, so you could bundle it out to a separate
stylesheet and include it rather than create it dynamically.  The only
thing that isn't static is the creation of the $selected-nodes
variable:</para>
	<programlisting>
&lt;xsl:variable name="selected-nodes"
              select="//product/@sku | //product/cost"/>
</programlisting>
<para>Assuming that you're using oxsl as the namespace prefix for your XSLT
namespace alias, then you need to create an oxsl:variable with a name
attribute equal to selected-nodes:</para>
	<programlisting>
&lt;oxsl:variable name="selected-nodes">
   ...
&lt;/oxsl:variable>
</programlisting>
<para>The select attribute is created dynamically by iterating over the
various expressions that you have (I'll assume their held in an $exprs
variable):</para>
	<programlisting>
&lt;oxsl:variable name="selected-nodes">
   &lt;xsl:attribute name="select">
      &lt;xsl:for-each select="$exprs">
         &lt;xsl:value-of select="." />
         &lt;xsl:if test="position() != last()"> | &lt;/xsl:if>
      &lt;/xsl:for-each>
   &lt;/xsl:attribute>
&lt;/oxsl:variable>
</programlisting>
<para>You should be able to create the rest of the stylesheet very
straight-forwardly.</para>


   </answer>
  </qandaentry>


 

 <qandaentry>
   <question>
    <para>Selecting  only one node value out of the many identical values</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
    <programlisting format="linespecific">

> this XML is genereted based on query from a user and comes from a
> database. I want to display to the user only the available "brands"
> of shirts Ie. output shall be only "Arrow" and "Lee" in my case. and
> the user can then further query on the subitems like "size" and
> "color". SO I want a  Select "brand" from "clothes/category/sno"
> where "there is no repetiion"
	</programlisting>
<para>Getting the 'distinct' values involves getting only those values that
haven't already occurred in the document.  The basic method of doing
this is to find all the nodes where there isn't a preceding node that
has the same value in the document.</para>
<para>
In your case you want all the sno elements:</para>
	<programlisting>
  clothes/category/sno
</programlisting>
<para>And then you want to filter in those where there aren't any preceding
(sibling) sno elements whose brand is the same as the brand of the
particular sno element:</para>
	<programlisting>
  clothes/category/sno[not(preceding-sibling::sno/brand = brand)]
</programlisting>
<para>If you have *lots* of sno elements, then another possibility is to
create a key that indexes the sno elements by brand:</para>
	<programlisting>
&lt;xsl:key name="sno-by-brand" match="sno" use="brand" />
</programlisting>
<para>You can then find all the sno elements with a particular brand (say
'Arrow') with the key() function:</para>
	<programlisting>
  key('sno-by-brand', 'Arrow')
</programlisting>
<para>and you can find all the sno elements that are first in the list of
sno elements of a particular brand, as returned by the key, with:</para>
	<programlisting>
  clothes/category/sno[generate-id() =
                       generate-id(key('sno-by-brand', brand)[1])]

or:

  clothes/category/sno[count(.|key('sno-by-brand', brand)[1]) = 1]
</programlisting>

   </answer>
  </qandaentry>
<qandaentry>
   <question>
    <para>Selecting all but two elements by name</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
   
 
  <para>It depends how much you care about namespaces.</para>
  
	<literallayout>    *[not(name()='a' or name()='b')]</literallayout>
  
  <para>Gives you all the elements aside from a and b elements that are in the
  default namespace in their document. I'd tend to steer clear of this
  because the default namespace could be anything at all.</para>
  
	<literallayout>    [not(self::a or self::b)]</literallayout>
  
  <para>Gives you all the elements aside from a and b elements that are in no
  namespace. Use this if the a and b elements that you want to ignore
  don't have a namespace or if you're not using namespaces at all.</para>
  
	<literallayout>    *[not(local-name()='a' or local-name()='b')]</literallayout>
  
  <para>Gives you all the elements aside from a and b elements in any
  namespace. Use this if you want to ignore all a and b elements no
  matter what namespace they're in.</para>
   </answer>
  </qandaentry>
 <qandaentry>
   <question>
    <para>Get the second para after the heading</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">
 > I'm trying to find a way of saying 'output the third paragraph after
 > the heading containing the text "foo"' -- to no avail.
</literallayout> 
 <para>It's probably easiest to do this by only selecting that paragraph
 rather than (as you are doing currently, it seems) trying to match
 only that paragraph.</para>
<para> 
 You can get the h1 whose value is 'Best Section' with:</para>
 
	<literallayout>   h1[. = 'Best Section']</literallayout>
 
 <para>and then get the second following sibling p element from that with:</para>
 
	<literallayout>   h1[. = 'Best Section']/following-sibling::p[2]</literallayout>
 
 <para>If you only apply templates to that p element, then that's the only
 output that you'll get.</para>
   </answer>
  </qandaentry>

  <qandaentry>
      <question>
	<para>Select all children except ...</para>
      </question>
      <answer>
	<para role="author">Joerg Pietschmann</para>
	<literallayout>

> I don't really know if "not(self::c)" is more efficient than "name()!='c'"
> but I prefer it for some reason, maybe because there's no function call?
	</literallayout>

<para>I don't think there is a performance difference for most XSLT processors.</para>

<para>However, using self:: is more robust when it comes to elements in a
namespace. If you write</para>
	<literallayout>
 name()='foo:bar'
</literallayout>
<para>only elements with the literal name foo:bar fulfill this condition but not
baz:bar, even if the baz prefix is bound to the same namespace.
Like in</para>

	<programlisting> &lt;sample xmlns:foo="uri:sample:stuff" xmlns:baz="uri:sample:stuff">
   &lt;foo:bar/>
   &lt;baz:bar/>
 &lt;/sample>
</programlisting>
<para>the inner elements have different a name(), but they are both matched
by self::foo:bar</para>

      </answer>
    </qandaentry>
  <qandaentry>
      <question>
	<para>Select an element based on childrens name</para>
      </question>
      <answer>
	<para role="author">Wendell Piez</para>
	<literallayout>
>>   if i want to select all elements that have at least one, say,
>>"book" child element, it seems i can do it this way:
>>   //*[name(child::*) = "book"]          long way
>>   //*[name(*) = "book"]                 shorter way
>>1) is this the easiest way to do it?
>
>//*[book]
>
>>2) why doesn't the following work as well?
>>   //*[name(node()) = "book"]
>
>i'm not sure, but i think this will compare the name of the first child only.
</literallayout>
<para>Yup. The name() function takes a node set as its argument, and returns the 
name of the first node in the node set.</para>

<para>All three of the name tests will fall into this category.</para>

<para>/*[book] is the way to do it, since the XPath expression "book" (short for 
child::book) returns true if there are any book child elements (not just if 
the first element child is a book)</para>
<para>
//*[book] is short for</para>
<programlisting>
/descendant-or-self::node()/child::*[child::book]</programlisting>
<para>
(Traversing from the root, get all nodes at any depth, then get any element 
child that has a book element child.)</para>
<para>
name(node())='book' might be seeming to fail where name(*)='book' seems to 
work due to whitespace in the input. If the first element child is a &lt;book> 
but it is preceded by a text node (even one that just contains a line 
feed), the test name(node())='book' on its parent will test false whereas 
name(*)='book' tests true.</para>
<para>
I hope this clarifies!</para>



      </answer>
    </qandaentry>

    <qandaentry>
      <question>
	<para>Selecting elements in a given namespace</para>
      </question>
      <answer>
	<para role="author">Jeni Tennison</para>
	<literallayout>

> if i want to select all nodes in a given namespace ("auth"), it
> appears i can use:
>
>   //namespace::auth
</literallayout>
<para>That will select all the *namespace*nodes* named "auth". Namespace
nodes are nodes that represent a prefix-namespace binding. Such a
namespace node will exist on every element within the scope of the
xmlns:auth="..." namespace declaration.</para>
<para>
If you want to select all the elements in the namespace associated
with the prefix 'auth', use:</para>

	<programlisting>  //auth:*
</programlisting>
<para>All attributes in that namespace with:</para>
	<programlisting>
  //@auth:*
</programlisting>
<para>Note that the 'auth' namespace must be declared in the stylesheet
(or in whatever context you're selecting the nodes). A stylesheet will
not automatically pick up on the fact that the namespace is declared
in the XML document.</para>
	<literallayout>
> so what is the XPath expression to match, say, the &lt;last-name>
> elements within this namespace?
</literallayout>
<para>XPath expressions don't match elements, they select them. If you want
to select the last-name element in the 'auth' namespace, use:</para>

	<programlisting>  //auth:last-name
</programlisting>
<para>or, better:</para>

	<programlisting>  /auth:author/auth:last-name
</programlisting>
<para>To *match* the auth:last-name element with an XSLT pattern, you can
use:</para>
	<programlisting>
  auth:last-name
</programlisting>
<para>Note that in your example, the &lt;last-name> element is not in the
namespace associated with the 'auth' prefix since it does not have a
prefix and there is no default namespace declaration. In your example,
only the &lt;auth:author> element is in the 'auth' namespace.</para>
  


      </answer>
    </qandaentry>
    <qandaentry>
      <question>
	<para>Selective processing of first n elements</para>
      </question>
      <answer>
	<para role="author">Passin, Tom</para>
	<literallayout>

> I can't seem to get something 
> which will only 
> print/process the first 'x' (in my case x is 2) elements and 
> then bail out 
> of the for-each or just simply ignore the remaining ones found.
> 
	</literallayout>


<para>This is a good chance for you to start getting into XSLT-think mode.
Instead of limiting a loop, think of selecting just the elements you
want.  The simplest example is to put this into a template -</para>
	<programlisting>
   &lt;xsl:apply-templates select='element-to-work-with[3 > position()]'/>
</programlisting>
<para>Here you select just the first two nodes, and so whatever template
processes them only ever sees those two nodes to work on.</para>
<para>
The most common and fundamental thing one usually does in xslt is to
select a set of nodes to work with.  Therefore, think carefully about
how to select exactly the nodes you want.</para>
<para>
[Note that the sense of the test is reversed to avoid having to write
&amp;lt;, which will work fine  but is harder to read]</para>

      </answer>
    </qandaentry>




    <qandaentry>
      <question>
	<para>Pick first few elements as file sample</para>
      </question>
      <answer>
	<para role="author">Dimitre Novatchev</para>
	<literallayout>
> Need a simple transformation to reduce the size of a file... so like I
> just need to see the first 10 elements of an xml source which is 10
> megs!  Those first 10 elements would be the first 10 child elements to
> the source including their child elements.  Here's what ISN'T working:
> 
> 
>    &lt;xsl:template match="node | @*">
>        &lt;xsl:apply-templates select="node[position() &amp;lt;= 10] | @*" />
>    &lt;/xsl:template>
</literallayout>

<para>Here's what is working:</para>
	  <programlisting>
&lt;xsl:stylesheet version="1.0" 
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 &lt;xsl:output omit-xml-declaration="yes" indent="yes"/>
 
 &lt;xsl:strip-space elements="*"/>
 
  &lt;xsl:template match="node()|@*">
    &lt;xsl:copy>
      &lt;xsl:apply-templates select="node()|@*"/>
    &lt;/xsl:copy>
  &lt;/xsl:template>
  
  &lt;xsl:template match="/*/*[position() > 10]"/>
  
&lt;xsl:template match="*[ancestor::*[3]]"/>

&lt;/xsl:stylesheet>

</programlisting>
      </answer>
    </qandaentry>

    <qandaentry>
      <question>
	<para>Select one from a number of items</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
	<literallayout>

> I have a gallery.xml file containing a number of images:
>
> &lt;gallery>
>     &lt;image>
>       &lt;filename>1.jpg&lt;/filename>
>     &lt;/image>
>     &lt;image>
>       &lt;filename>2.jpg&lt;/filename>
>     &lt;/image>
>     &lt;!-- etc -->
>     &lt;image default="true">
>       &lt;!-- optional attribute applies to this element -->
>       &lt;filename>10.jpg&lt;/filename>
>     &lt;/image>
> &lt;/gallery>
>
> and my xslt has to do the following thing -- look for an
> image marked with a "default" attribute, and if it's not
> found, use the first image instead. So I'm looking for the
> position number of the marked element, and if I can't find
> it, using 1:
</literallayout>

<para>Try</para>

<programlisting>&lt;xsl:copy-of select="(image[1] | image[@default='true'])[last()]"/></programlisting>


      </answer>
    </qandaentry>

    <qandaentry>
      <question>
	<para>Namespace nodes.</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
	<literallayout>



> Question regarding "&lt;xsl:apply-templates 
> select="*"/>". 
> Is this going to be applied for each node and attribute > also? 
> So for eg:
>  if I have &lt;ABC xmnls:ns2="http://"> would "&lt;template 
>  match=*>" be called for ABC and then xmnls:ns2 also.
</literallayout>
<para>
1. select="*" only selects child elements, it does does not select
attributes, namespaces, text nodes, comments, or processing instructions.</para>
<para>
2. xmlns:ns2 in the XPath model is not an attribute. It turns into a
namespace node. You cannot apply-templates to namespace nodes (well, you
can, but no template rule will ever match them).</para>
<para>
To get rid of namespace nodes that are being copied from the source
document, in XSLT 2.0 you can use &lt;xsl:copy copy-namespaces="no">. In 1.0
you have to use &lt;xsl:element name="{name()}" namespace="{namespace-uri()}">.</para>

      </answer>
    </qandaentry>

</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
