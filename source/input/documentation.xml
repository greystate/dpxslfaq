<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-02-22 07:34:58 dpawson"   -->

<!DOCTYPE webpage  SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="documentation">

<head>
<title>Documentation </title>
<summary>Self Documenting Code</summary>
</head>
<qandaset>

 <qandaentry>
   <question>
    <para>Literate Programming</para>

   </question>
   <answer>
    <para role="author">Tony Coates</para>
    

<para>Literate Programming is about embedding code fragments in
human-readable documentation (rather than the usual reverse situation)
so that the information is presented is the order which best suits
people, rather than the order which best suits compilers. For more
information about literate programming, see

	  <ulink url="http://www.literateprogramming.com/">literateprogramming.com</ulink></para>

<para>xmLP is a literate programming tool for XML, written as a set of XSL-T
	  scripts. Version 1.0 is now available from :<ulink url="http://xmLP.sourceforge.net/">SourceForge</ulink></para>


   </answer>
  </qandaentry>


  <qandaentry>
   <question>
    <para>Documenting Stylesheets</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">

> Anyone out there have opinions on what things are the most necessary
> to comment in a stylesheet, and what things can best be left out.
	</literallayout>

<para>I agree about basic template-level comments being more useful than
code-level comments.</para>
<para>
The main comments that I find helpful are those concerning the
assumptions that have been made in the stylesheet about the structure
of the source document. For example, if you have an xsl:choose like:</para>
	<programlisting>
  &lt;xsl:choose>
    &lt;xsl:when test=". = 'value1'">
      ...
    &lt;/xsl:when>
    &lt;xsl:when test=". = 'value2'">
      ...
    &lt;/xsl:when>
  &lt;/xsl:choose>
</programlisting>
<para>then it's helpful to know whether value1 and value2 are the only
possible values, or if there are other possibilities that are
(correctly) being ignored. Or if you have a plain xsl:apply-templates,
with no select attribute, it's helpful to know what kinds of nodes
you're assuming will be processed by that instruction (just
descriptors like "inline elements" rather than listing them all). Or a
comment that indicates the expected type of a parameter. These kinds
of comments make following the flow of the stylesheet easier, and they
help identify areas that might need to change when/if the source
markup language changes.</para>
<para>
Other comments that I think can be helpful are those that explain
complex XPath expressions in plain language. So things like:</para>
	<programlisting>
  &lt;!-- $rows holds a node set of unique row elements, by their code
       attribute -->
  &lt;xsl:variable name="rows"
    select="//row[count(.|key('rows', @code)[1]) = 1]" />
</programlisting>
<para>XPath 2.0 allows comments *within* expressions (using the syntax {--
... --}), which would mean you could do:</para>
	<programlisting>
  &lt;xsl:variable name="rows"
    select="//row[count(.|key('rows', @code)[1]) = 1]
            {-- select unique row elements by their code attribute --}" />
</programlisting>
<para>You can see why this might be necessary, given that XPath expressions
will contain for and if statements...</para>
<para>
The other thing to mention is that you can go quite a long way towards
making a stylesheet more readable and understandable without adding
comments all over the place, just by using helpful variable names, key
names, mode names, template names and so on.</para>

<para>(P.S. I wrote an article about documentation for the "XPath &amp; XSLT on
the Edge unlimited edition" website
(http://www.unlimited-edition.com/), but it doesn't seem to have
appeared yet...) - April 2002</para>

   </answer>
  </qandaentry>
<qandaentry>
 <question>
   <para>Documenting XSLT code</para>
 </question>
 
<answer>
    <para role="author">Jeni Tennison / David Carlisle</para>
    <para>

 I think that David Carlisle's stylesheet was meant to
do two things:</para>

    <para>1. demonstrate how extension elements (the ones in the doc namespace) could
be used to hold documentation that would be ignored by the processor, with
an *empty* xsl:fallback being used to stop processors complaining that they
don't recognise the extension element (it gives them something to do instead)
</para>
    <para>2. demonstrate the effect of exclude-result-prefixes on the appearance of
namespace nodes (and elements within that namespace) within the output
that's generated
</para>
    <para>I'm going to try to clear up my confusion by going through David's example
step by step.  I'm afraid this is going to be another one of my long emails
- - please someone shout at me if I'm using up too much bandwidth here.
</para>
    <para>David used:
</para>
    <programlisting>&lt;xxx xmlns="http://a/b/c">
  &lt;b a="2">
    &lt;x:c xmlns:x="http:/x/y/z"/>
  &lt;c>text&lt;/c>
  &lt;/b>
&lt;/xxx>
</programlisting>
    <para>as the example of a fairly complex, but nevertheless short, XML document
that used a couple of namespaces and:
</para>
    <programlisting>&lt;?xml version="1.0" encoding="utf-8" ?>
&lt;x xmlns:y="http:/x/y/z">[xxx]&lt;/x>
&lt;x:c xmlns:x="http:/x/y/z" xmlns="http://a/b/c"/>
</programlisting>
    <para>as the (non-well-formed) output that he was after.
</para>
    <programlisting>The stylesheet included:

&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                extension-element-prefixes="doc"
                exclude-result-prefixes="main"
                xmlns:doc="http://doc.com"
                xmlns:main="http://a/b/c"
                xmlns:y="http:/x/y/z">
</programlisting>
    <programlisting>Here he declared three namespaces (in addition to the standard 'xsl'):

doc  - the documentation namespace
main - the primary namespace, the default namespace throughout the input
       document
y    - the namespace equivalent to 'x' for the x:c element in the input
</programlisting>
    <para>Mapping those namespace declarations so that the same prefixes are used in
the input gives helps make the XSLT stylesheet more understandable:
</para>
    <programlisting>&lt;main:xxx xmlns:main="http://a/b/c">
  &lt;main:b a="2">
    &lt;y:c xmlns:y="http:/x/y/z"/>
    &lt;main:c>text&lt;/main:c>
  &lt;/main:b>
&lt;/main:xxx>
</programlisting>
    <para>The 'main' namespace is specified as a namespace to be excluded from the
result.  This means that the 'main' namespace will be declared as being the
default namespace in the output (so wherever the result elements are in the
'main' namespace within the stylesheet, they will be included in the output
without a prefix).
</para>
    <para>The 'doc' namespace is specified as a namespace for extension elements.
You'd normally expect those elements to do something in terms of altering
what the XSLT processor does (like saxon:output).  Here, we're declaring
the 'doc' namespace to be a namespace for extension elements so that the
XSLT processor doesn't treat them as result elements, to be outputted.
</para>
    <para>David's 'doc' namespace included two elements: 'template' to give
documentation on templates and 'select' to give documentation on select
expressions within templates.  He didn't have any particular structure
within those, though you can easily imagine having structured documentation
within the documentation on a template, commenting on, say, each of the
parameters that it takes.  In fact I'm sure someone will straightway
volunteer an XML dialect for documentation of XSLT stylesheets...
</para>
    <para>Anyway, within the stylesheet, then, he used the following 'design pattern'
for including documentation within a template:
</para>
    <programlisting>&lt;xsl:template match="...">
  &lt;doc:template>&lt;xsl:fallback />
    &lt;!-- documentation on the template -->
  &lt;/doc:template>
  &lt;!-- body of the template -->
&lt;/xsl:template>
</programlisting>
    <para>When an extension-element-aware (and XSLT-Recommendation-compliant)
processor comes across this, it knows (because it's been told in the
xsl:stylesheet) that the 'doc:template' element, being in the 'doc'
namespace, is an extension element.  It does a bit of introspection and
finds out that it doesn't know how to support these extensions, so it gets
ready to complain that it can't process the stylesheet, but first checks
whether there is an xsl:fallback element within the extension element.
When it finds one, it operates on the content of the xsl:fallback as if it
were just a normal part of the template (here it's empty, so it does
nothing), and ignores the rest of the content of the extension element.
Hence the documentation is not included in the output.
</para>
    <para>The fact that an XSLT processor ignores the content of extension elements
like this is important because it means that you can put anything you like
in it and it won't be processed.  This includes any XSLT elements that you
include in it.  There is no point in doing:
</para>
    <programlisting>  &lt;doc:template>&lt;xsl:fallback />
    This template matches &lt;xsl:value-of select="name()" />.
  &lt;/doc:template>
</programlisting>
    <para>because the xsl:value-of element is never processed (by an XSLT processor
that doesn't understand doc:template).
</para>
    <para>David points out that the fact that an XSLT processor does process the
content of the xsl:fallback element is important because it means you could
use it to contain the stuff that you're actually commenting on.  So, you
could use:
</para>
    <programlisting>  &lt;xsl:template match="...">
    &lt;doc:template>
      &lt;!-- documentation on the template -->
      &lt;xsl:fallback>
        &lt;!-- body of the template -->
      &lt;/xsl:fallback>
    &lt;/doc:template>
  &lt;/xsl:template>
</programlisting>
    <para>as the design pattern for including documentation instead.  In a previous
email, I've pointed out that this probably isn't a good idea just in case
some XSLT processor comes along that *does* understand the 'doc:template'
extension element, because then the content of the xsl:fallback element
(which is the content of the template) will not be processed, and your
legacy stylesheet won't work.
</para>
    <para>The point, of course, of using XML to represent the documentation within
the stylesheet is that you can get at it with things that understand XML,
most importantly XSLT.  So, you can then have another XSLT stylesheet that
takes our documented XSLT stylesheet as input and produces a nice HTML
frameset that explain what the stylesheet does and how it works.  I'm sure
whoever volunteers the XML dialect for documentation will include such a
stylesheet in their contribution...
</para>
    <para>What the documentation included in this way *doesn't* do, cannot do, and
isn't really designed to do, is perform any run-time evaluation of the
performance of the stylesheet.  To do that, the documentation would need to
be understandable by an XSLT processor.  It would be possible to extend an
XSLT processor so that it understood the extension elements in a particular
namespace that was used for documentation (in this case "http://doc.com"),
and did something clever with them.  I'm not exactly sure what it could do
other than generate messages in a similar way to xsl:message, but I'm sure
there are plenty of ideas out there.  And I'm sure that someone will
volunteer to write extensions for SAXON that will carry them through...
</para>
   </answer>
  </qandaentry>


<qandaentry>
 <question>
   <para>Documenting XSLT stylesheets</para>
 </question>
 
<answer>
    <para role="author">Warren Hedley, David Carlisle, DaveP</para>
    <para>


(for a background on tangle, weave, David Carlisle suggests
This terminology (which is pretty bad, really) was introduced by
Knuth in his work on "literate programming" (and specifcally in the
construction of the TeX typesetting system).</para>

    <para> There is a newsgroup just for this topic (which I haven't looked at for
a while:-) but its FAQ appears to be here (among other places)

     <ulink url="ftp://ftp.cs.uu.nl/pub/NEWS.ANSWERS/literate-programming-faq">FAQ</ulink> )</para>


    <para>Three files below.</para>

    <simplelist>
     <member>A 'self documented stylesheet' (doc.xsl)</member>
     <member>A stylesheet (doc-doc.xsl), which works on doc.xsl
    to extract the documentation in the doc namespace</member>
     <member>A stylesheet doc-logic.xsl, which works on the doc.xsl
   to extract the code.
</member></simplelist>
    <para>Usage:
 1. To extract the code</para>
     <programlisting>   (xslt processor) doc.xsl doc-logic.xsl op.html \
     "logic-ns=http://www.dpawson.co.uk" \ 
     "doc-ns=http://org.hedley.html"</programlisting>

    <para>These are the namespaces used in doc.xsl, but use what you will.</para>

    <para> To extract the documentation.</para>
     <programlisting>(xslt processor) doc.xsl doc-doc.xsl op.html \
     "logic-ns=http://www.dpawson.co.uk" \ 
     "doc-ns=http://org.hedley.html"</programlisting>

    <para>To do.</para>

    <programlisting>1. Add coloring (preferably via CSS) to any wanted elements in doc-doc
2. Generalise to any namespaces for any purpose.
</programlisting>
    <para>Enjoy and let us know what you think.
Regards, DaveP, on behalf of.</para>

     <para>1. doc.xsl</para>
<programlisting>

&lt;?xml version="1.0"?>
 &lt;!--  -->
 &lt;!-- This is the 'source document' -->
 &lt;!-- Containing both documentation and 'code' -->
  

   &lt;logic:stylesheet 
     xmlns:doc="http://org.hedley.html"
     xmlns:logic="http://www.dpawson.co.uk"
  >

 &lt;doc:h1>Root Element&lt;/doc:h1>
    &lt;doc:p>My root element doesn't really do anything useful.&lt;/doc:p>

  &lt;logic:output method="xml" indent="yes"/>
  &lt;doc:h3>Variable &lt;doc:i>root&lt;/doc:i> holds the src tree&lt;/doc:h3>
  &lt;logic:variable name="root" select="/"/>


  &lt;logic:template match="/">
    &lt;logic:apply-templates/>
  &lt;/logic:template>


  &lt;doc:hr/>
    &lt;doc:h2>Main processing.&lt;/doc:h2>
    &lt;doc:p>This template uses a pull technique to extract all required
elements from the source document required by the output xml file&lt;/doc:p>
    &lt;doc:p>Process all the related list section elements&lt;/doc:p>
  &lt;logic:template match="doc">
    &lt;elems id="h31">
      &lt;logic:for-each select="listrelsects/listitem">
	&lt;elem>&lt;logic:copy-of select="."/>&lt;/elem>
      &lt;/logic:for-each>
    &lt;/elems>
  &lt;/logic:template>
  &lt;doc:hr/>
  &lt;doc:h3>Ignore remaining elements&lt;/doc:h3>
  &lt;logic:template match="*"/>
  

&lt;/logic:stylesheet>
</programlisting>
<para> doc-logic.xsl  </para>
   <programlisting>


&lt;?xml version="1.0"?>
&lt;xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
>
&lt;!-- 
A command line might look like this 

xslt-processor \
 doc.xsl \        # in
  doc-logic.xsl \     # transform
  logic_ns=http://www.dpawson.co.uk  \ # the logic namespace
  doc_ns=http://org.hedley.html   # the documentation namespace
  e.g. sax doc.xsl doc-logic.xsl op.xsl "logic_ns=http://www.dpawson.co.uk"
\
   output_ns="org.hedley.html"

- -->

&lt;!--
  This one outputs basic code to perform the styling.
 
- -->

&lt;xsl:param name="logic_ns"    select="'***'" />
&lt;xsl:param name="doc_ns"   select="'***'" />

&lt;xsl:variable name="xsl_ns"
    select="'http://www.w3.org/1999/XSL/Transform'" />

  &lt;xsl:output method="xml" indent="yes"/>
  &lt;xsl:strip-space elements="*"/>


  &lt;xsl:template match="/">
    &lt;xsl:message>Param=&lt;xsl:value-of select="$doc_ns"/>
  &lt;/xsl:message>
    &lt;xsl:apply-templates/>
  &lt;/xsl:template>


  &lt;xsl:template match="*" priority="2">
  
  &lt;xsl:choose>
&lt;!-- Documentation namespace, dump -->
    &lt;xsl:when  test="namespace-uri(.) = $doc_ns">
      &lt;xsl:message>Doc. &lt;xsl:value-of select="namespace-uri()"/>
    &lt;/xsl:message>
      &lt;/xsl:when>
&lt;!-- output, logic namespace -->
      &lt;xsl:when test="namespace-uri()=$logic_ns">
	&lt;xsl:element name="xsl:{local-name()}" namespace="{$xsl_ns}">
	  &lt;xsl:copy-of select="@*"/>
	 &lt;xsl:apply-templates/>
       &lt;/xsl:element>
	&lt;/xsl:when>
&lt;!-- null namespace -->
	&lt;xsl:when test="namespace-uri()=''">
	  &lt;xsl:copy>
	    &lt;xsl:copy-of select="@*"/>
	  &lt;xsl:apply-templates/>	
	  &lt;/xsl:copy>
      &lt;/xsl:when>
	&lt;xsl:otherwise>
	
	&lt;/xsl:otherwise>
      &lt;/xsl:choose>
  &lt;/xsl:template>
&lt;/xsl:stylesheet>
</programlisting> 


     <para>3. doc-doc.xsl
- - Tks to David C for the 'hard-sums' code :-)</para>

     <programlisting>
&lt;?xml version="1.0"?>
&lt;!-- Update
7 July 2000. Added null processing for stylesheet element,
as being redundant.

 -->
 &lt;!-- 
A command line might look like this 


   sax doc.xsl doc-doc.xsl op.xml "doc_ns=http://org.hedley.html"
"logic_ns=http://www.dpawson.co.uk"



- -->


&lt;!DOCTYPE xsl:stylesheet [
&lt;!ENTITY sp "&lt;xsl:text> &lt;/xsl:text>">
&lt;!ENTITY dot "&lt;xsl:text>.&lt;/xsl:text>">
&lt;!ENTITY nbsp "&#160;">
&lt;!ENTITY nl "&#xa;">&lt;!--new line-->
&lt;!ENTITY  pound	"&#x00A3;">&lt;!--	# POUND SIGN -->

]>
&lt;xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
 
                >

&lt;!--
  This one outputs basic code to perform the styling.
 
- -->
&lt;xsl:param name="logic_ns"    select="'***'" />
&lt;xsl:param name="output_ns"   select="'***'" />
&lt;xsl:param name="doc_ns"      select="doc-format"/>

&lt;xsl:variable name="xsl_ns"
    select="'http://www.w3.org/1999/XSL/Transform'" />

  &lt;xsl:output method="xml" indent="yes"/>


  &lt;xsl:template match="/">
    &lt;html>
      &lt;head>&lt;title>Documentation&lt;/title>&lt;/head>
      &lt;body>
	&lt;xsl:apply-templates/>
      &lt;/body>
    &lt;/html>
  &lt;/xsl:template>



  &lt;xsl:template match="*" priority="2">
    
    &lt;xsl:choose>
&lt;!-- Document Namespace. -->
    &lt;xsl:when  test="namespace-uri(.) = $doc_ns">
      &lt;xsl:element name="{local-name(.)}">
	&lt;xsl:copy-of select="@*" />
	&lt;xsl:apply-templates />
      &lt;/xsl:element>
    &lt;/xsl:when>
    &lt;xsl:when test="namespace-uri()=$logic_ns">
      &lt;xsl:if test= "not(local-name()='stylesheet')">
	&lt;pre>
	  &lt;xsl:apply-templates mode="verb" select="."/>
	&lt;/pre>
      &lt;/xsl:if>
      &lt;xsl:apply-templates/>
    &lt;/xsl:when>
    &lt;xsl:when test="namespace-uri()=''"/>
    &lt;pre>
      &lt;xsl:apply-templates mode="verb" />
    &lt;/pre>
    &lt;xsl:apply-templates />
    &lt;xsl:otherwise>
      &lt;xsl:apply-templates />
    &lt;/xsl:otherwise>
  &lt;/xsl:choose>
&lt;/xsl:template>



&lt;!-- Copyright 1999 David Carlisle

     Render XML in an HTML pre element. -->

&lt;!--   verb mode -->

&lt;!-- Does not really give verbatim copy of the file as that
     information not present in the parsed document, but should
     give something that renders in HTML as a well formed XML
     document that would parse to give same XML tree as the original
- -->


&lt;!-- DaveP. Added prefix and postfix to replace logic_ns prefix
with xsl: 
Called from both empty elements and those with content.
 -->
  &lt;xsl:template name="prefix">
  &lt;xsl:choose>
    &lt;xsl:when test="namespace-uri(.)=$logic_ns">
      &lt;xsl:value-of select="concat('&lt;xsl:',local-name(.))"/>
    &lt;/xsl:when>
    &lt;xsl:otherwise>
      &lt;xsl:value-of select="concat('&lt;',name(.))"/>
    &lt;/xsl:otherwise>
  &lt;/xsl:choose>
&lt;/xsl:template>

&lt;xsl:template name="postfix">
 &lt;xsl:choose>
    &lt;xsl:when test="namespace-uri(.)=$logic_ns">
      &lt;xsl:value-of select="concat('&lt;/xsl:',local-name(.),'&gt;')"/>
    &lt;/xsl:when>
    &lt;xsl:otherwise>
      &lt;xsl:value-of select="concat('&lt;/',name(.),'&gt;')"/>
    &lt;/xsl:otherwise>
  &lt;/xsl:choose>

&lt;/xsl:template>



&lt;!-- non empty elements and other nodes. -->
&lt;xsl:template mode="verb"
match="*[*]|*[text()]|*[comment()]|*[processing-instruction()]">

&lt;xsl:call-template name="prefix"/>
  &lt;!--  &lt;xsl:value-of select="concat('&lt;',name(.))"/> -->
   &lt;xsl:apply-templates mode="verb" select="@*"/>
   &lt;xsl:text>&gt;&lt;/xsl:text>
   &lt;xsl:apply-templates mode="verb"/>
&lt;xsl:call-template name="postfix"/>
&lt;!-- &lt;xsl:value-of select="concat('&lt;/',name(.),'&gt;')"/> -->
 &lt;/xsl:template>

&lt;!-- empty elements -->
&lt;xsl:template mode="verb" match="*">

  &lt;!--  &lt;xsl:value-of select="concat('&lt;',name(.))"/> -->
    &lt;xsl:call-template name="prefix"/>

  &lt;xsl:apply-templates mode="verb" select="@*"/>
  &lt;xsl:text>/&gt;&lt;/xsl:text>
&lt;/xsl:template>

&lt;!-- attributes
     Output always surrounds attribute value by "
     so we need to make sure no literal " appear in the value  -->
&lt;xsl:template mode="verb" match="@*">
  &lt;xsl:value-of select="concat(' ',name(.),'=')"/>
  &lt;xsl:text>"&lt;/xsl:text>
  &lt;xsl:call-template name="string-replace">
    &lt;xsl:with-param name="from" select="'&quot;'"/>
    &lt;xsl:with-param name="to" select="'&amp;quot;'"/> 
    &lt;xsl:with-param name="string" select="."/>
  &lt;/xsl:call-template>
  &lt;xsl:text>"&lt;/xsl:text>
&lt;/xsl:template>

&lt;!-- pis -->
&lt;xsl:template mode="verb" match="processing-instruction()">
  &lt;xsl:value-of select="concat('&lt;?',name(.),' ',.,'?&gt;')"/>
&lt;/xsl:template>

&lt;!-- only works if parser passes on comment nodes -->
&lt;xsl:template mode="verb" match="comment()">
  &lt;xsl:value-of select="concat('&lt;!--',.,'--&gt;')"/>
&lt;/xsl:template>

&lt;!-- text elements
     need to replace &amp; and &amp;lt; by entity references
     do > as well,  just for balance -->
&lt;xsl:template mode="verb" match="text()">
  &lt;xsl:call-template name="string-replace">
    &lt;xsl:with-param name="to" select="'&amp;gt;'"/>
    &lt;xsl:with-param name="from" select="'&gt;'"/> 
    &lt;xsl:with-param name="string">
      &lt;xsl:call-template name="string-replace">
        &lt;xsl:with-param name="to" select="'&amp;lt;'"/>
        &lt;xsl:with-param name="from" select="'&lt;'"/> 
        &lt;xsl:with-param name="string">
          &lt;xsl:call-template name="string-replace">
            &lt;xsl:with-param name="to" select="'&amp;amp;'"/>
            &lt;xsl:with-param name="from" select="'&amp;'"/> 
            &lt;xsl:with-param name="string" select="."/>
          &lt;/xsl:call-template>
        &lt;/xsl:with-param>
      &lt;/xsl:call-template>
    &lt;/xsl:with-param>
  &lt;/xsl:call-template>
&lt;/xsl:template>


&lt;!-- end  verb mode -->

&lt;!-- replace all occurences of the character(s) `from'
     by the string `to' in the string `string'.-->
&lt;xsl:template name="string-replace" >
  &lt;xsl:param name="string"/>
  &lt;xsl:param name="from"/>
  &lt;xsl:param name="to"/>
  &lt;xsl:choose>
    &lt;xsl:when test="contains($string,$from)">
      &lt;xsl:value-of select="substring-before($string,$from)"/>
      &lt;xsl:value-of select="$to"/>
      &lt;xsl:call-template name="string-replace">
      &lt;xsl:with-param name="string"
select="substring-after($string,$from)"/>
      &lt;xsl:with-param name="from" select="$from"/>
      &lt;xsl:with-param name="to" select="$to"/>
      &lt;/xsl:call-template>
    &lt;/xsl:when>
    &lt;xsl:otherwise>
      &lt;xsl:value-of select="$string"/>
    &lt;/xsl:otherwise>
  &lt;/xsl:choose>
&lt;/xsl:template>
&lt;/xsl:stylesheet>
</programlisting>





 
   </answer>
  </qandaentry>

  <qandaentry>
   <question>
    <para>Documenting XSLT</para>

   </question>
   <answer>
    <para role="author"> Tony B. Coates</para>
    <para>

I have been working (albeit slowly) on a system for doing literate programming
from XML source documents.  I call the tool "xmLP"; it is still at an "alpha"
stage, but nonetheless does both "weave" (generate documentation) and "tangle"
(assemble the product source files).  At the bottom of this message is a sample
of the documentation produced (currently) by "xmLP".  This is based heavily on
my FunnelWeb experience.</para>


    <para>PS If you have questions about literate programming &amp; XML, you could do worse
than join the (very low volume) 
     <ulink url="http://www.egroups.com/group/xml-litprog-l">"xml-litprog-l"</ulink> list.
</para>


    <para>{ URL to follow when the code is ready - Juli 2000)}
</para>


   </answer>
  </qandaentry>



 

   
 <qandaentry>
   <question>
    <para>Xsldoc - XSLT Source Code Documentation Tool</para>

   </question>
   <answer>
    <para role="author">Rick Maddy</para>
    <para>


Announcing Xsldoc 0.9.</para>

<para>This tool is for generating documentation of your XSLT files. The output is
similar to Javadoc output for Java source files. The same commenting tags
are used as well.</para>
<para>
	  See <ulink url="http://www.xsldoc.org">xsldoc</ulink> for more info and to download this free software.
</para>
   </answer>
  </qandaentry>









 




  <qandaentry>
   <question>
    <para>Documenting Stylesheets</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">

> Anyone out there have opinions on what things are the most necessary
> to comment in a stylesheet, and what things can best be left out.
	</literallayout>

<para>I agree about basic template-level comments being more useful than
code-level comments.</para>

<para>The main comments that I find helpful are those concerning the
assumptions that have been made in the stylesheet about the structure
of the source document. For example, if you have an xsl:choose like:</para>

	<programlisting>  &lt;xsl:choose>
    &lt;xsl:when test=". = 'value1'">
      ...
    &lt;/xsl:when>
    &lt;xsl:when test=". = 'value2'">
      ...
    &lt;/xsl:when>
  &lt;/xsl:choose>
</programlisting>
<para>then it's helpful to know whether value1 and value2 are the only
possible values, or if there are other possibilities that are
(correctly) being ignored. Or if you have a plain xsl:apply-templates,
with no select attribute, it's helpful to know what kinds of nodes
you're assuming will be processed by that instruction (just
descriptors like "inline elements" rather than listing them all). Or a
comment that indicates the expected type of a parameter. These kinds
of comments make following the flow of the stylesheet easier, and they
help identify areas that might need to change when/if the source
markup language changes.</para>
<para>
Other comments that I think can be helpful are those that explain
complex XPath expressions in plain language. So things like:</para>
	<programlisting>
  &lt;!-- $rows holds a node set of unique row elements, by their code
       attribute -->
  &lt;xsl:variable name="rows"
    select="//row[count(.|key('rows', @code)[1]) = 1]" />
</programlisting>
<para>XPath 2.0 allows comments *within* expressions (using the syntax {--
... --}), which would mean you could do:</para>

	<programlisting>  &lt;xsl:variable name="rows"
    select="//row[count(.|key('rows', @code)[1]) = 1]
            {-- select unique row elements by their code attribute --}" />
</programlisting>
<para>You can see why this might be necessary, given that XPath expressions
will contain for and if statements...</para>

<para>The other thing to mention is that you can go quite a long way towards
making a stylesheet more readable and understandable without adding
comments all over the place, just by using helpful variable names, key
names, mode names, template names and so on.</para>


<para>(P.S. I wrote an article about documentation for the "XPath &amp; XSLT on
the Edge unlimited edition" website
(http://www.unlimited-edition.com/), but it doesn't seem to have
appeared yet...)</para>

   </answer>
  </qandaentry>






  <qandaentry>
   <question>
    <para>Documenting stylesheets</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">
> The discussion now get me a little confused.
> Where lays the problem with:
>
>> &lt;xsl:stylesheet version="1.0"
>>                 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>>                 xmlns:doc="doc:documentation"
>>                 extension-element-prefixes="doc">
>> 
>> &lt;doc:module>Here's some documentation of my stylesheet&lt;/doc:module>
>> 
>> &lt;xsl:template match="/">
>>   &lt;doc:template name="templateName" match="whatever">
>>      &lt;doc:descr>And I can use it within templates too!&lt;/doc:descr>
>>      &lt;doc:result type="tNode-set">result description&lt;/doc:result>
>>      &lt;doc:param name="someName">Param description&lt;/doc:param>
>>      &lt;doc:param name="someOtherName">Param description&lt;/doc:param>
>>      ...............
>>      &lt;xsl:fallback />
>>   &lt;/doc:template>
>>   ...
>> &lt;/xsl:template>
>
> if i understand correctly, this a good approach, because
>
> 1. i can code a documenting stylesheet to build a resulting
> documentation of my application stylesheet
>
> 2. everything with the namespace doc is not outputted from my
> application stylesheet. Need i for this purpose:
> exclude-result-prefixes="doc" ?
	</literallayout>
<para>No -- the doc:* elements are labelled as being extension elements
through the extension-element-prefixes attribute. That means that the
namespace is automatically excluded from the result tree. However, it
also means that the processor will try to interpret the elements as
instructions, which leads on to...</para>
	<literallayout>
> Can anyone explain please for what is &lt;xsl:fallback /> in
> &lt;doc:template> ?
</literallayout>
<para>When a processor encounters the doc:template element, because it's
labelled as an extension element, the processor will interpret it as
an instruction. If it doesn't understand the instruction, it will
usually come out with an error. However, if you place an xsl:fallback
element inside, it won't raise an error, but rather will try to
process the content of the xsl:fallback element instead. (If the
xsl:fallback element is empty, then it does nothing, of course.)</para>
<para>
To give another, more common, example, say you were using Saxon's
saxon:group extension element:</para>
	<programlisting>
&lt;xsl:template match="people">
  &lt;saxon:group select="person" group-by="surname">
    ...
  &lt;/saxon:group>
&lt;/xsl:template>
</programlisting>
<para>If you ran that with MSXML, you'd get an error because it doesn't
understand the saxon:group element. To prevent that, you can add an
xsl:fallback which either halts processing with a helpful error
message:</para>
	<programlisting>
&lt;xsl:template match="people">
  &lt;saxon:group select="person" group-by="surname">
    ...
    &lt;xsl:fallback>
      &lt;xsl:message terminate="yes">
        You must use Saxon to run this stylesheet!
      &lt;/xsl:message>
    &lt;/xsl:fallback>
  &lt;/saxon:group>
&lt;/xsl:template>
</programlisting>
<para>or provides some alternative XSLT that does the same thing (more or
less) as the saxon:group. For example:</para>
	<programlisting>
&lt;xsl:template match="people">
  &lt;saxon:group select="person" group-by="surname">
    ...
    &lt;xsl:fallback>
      &lt;xsl:for-each select="person">
        &lt;xsl:sort select="surname" />
        ...
      &lt;/xsl:for-each>
    &lt;/xsl:fallback>
  &lt;/saxon:group>
&lt;/xsl:template>
</programlisting>
<para>Using xsl:fallback in an element that you're using for documentation
is a hack that enables you to take advantage of the fact that
extension elements don't get output in the result tree, but it's a
nasty hack, and a confusing use of xsl:fallback, which is why I think
that we should have something that's specific for documentation
instead, such as:</para>
	<programlisting>
&lt;xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:doc="doc:documentation"
                documentation-element-prefixes="doc">

&lt;doc:module>Here's some documentation of my stylesheet&lt;/doc:module>

&lt;xsl:template match="/">
  &lt;doc:template name="templateName" match="whatever">
     &lt;doc:descr>And I can use it within templates too!&lt;/doc:descr>
     &lt;doc:result type="tNode-set">result description&lt;/doc:result>
     &lt;doc:param name="someName">Param description&lt;/doc:param>
     &lt;doc:param name="someOtherName">Param description&lt;/doc:param>
     ...............
  &lt;/doc:template>
  ...
&lt;/xsl:template>

&lt;/xsl:stylesheet>

</programlisting>
   </answer>
  </qandaentry>
 


    <qandaentry>
      <question>
	<para>Adding documentation inside a template</para>
      </question>
      <answer>
	<para role="author">David Carlisle</para>
	<literallayout>

 Often times you need to include documentation
  that is not at the top level.
  </literallayout>
<para>
declare the documentation namespace to be an extension namespace and add an
empty xsl:fallback child.</para>
	<programlisting>
&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xsl:extension-element-prefixes="d "
                version="2.0">
&lt;d:doc xmlns:d="rnib.org.uk/tbs#">


 &lt;xsl:template match="/">
    &lt;d:documentation>

      This is documentation within a template
      &lt;xsl:fallback/>
    &lt;/d:documentation>

&lt;/xsl:template>
</programlisting>
      </answer>
    </qandaentry>
 <qandaentry>
      <question>
	<para>Documentation method for XSLT</para>
      </question>
      <answer>
	<para role="author">G Ken Holman</para>

<para>a documentation methodology for XSLT stylesheets, and I have
  (finally!) prepared a major update: <ulink
  url="http://www.CraneSoftwrights.com/resources/#xslstyle">link</ulink>
  The XSLStyle vocabulary sits at the top-level of your XSLT
  stylesheet, between all of the top-level XSLT constructs,
  documenting each top-level construct.</para>

<para>XSLStyle is only scaffolding, however, as the actual paragraphs, lists, figures, tables, and other constructs are authored by you in your choice of one of three vocabularies:</para>

<literallayout>  - HTML/XHTML
  - DITA
  - DocBook</literallayout>

<para>You can use as much of the embedded vocabulary you need inside of the scaffolding vocabulary.</para>

<para>You can then format the documentation to HTML and, something new in this delivery, engage CSS stylesheets to enhance the presentation that has been rather dull (though effective!) since its release.  Many thanks to Florent Georges, George Bina and Dominic Marcotte who have supplied two examples of CSS formatting of the report results.</para>

<para>During development of your stylesheets you can drag your stylesheet onto a browser and have the browser render the documentation as it stands.  Then, as you edit your XSLT stylesheet, you can simply save the file and refresh the browser in order to review your updated documentation.  When complete you can invoke an XSLT process on your XSLT stylesheet to create standalone HTML documentation.</para>

<para>XSLStyle implements a set of "stylesheet writing rules" that I have for myself, which you are welcome to use, or hack out, or ignore, or augment with your own rules.  Such rules include mandatory documentation on all template rules, functions and all parameters to them.</para>

<para>All stylesheet fragments in your XSLT stylesheet import/include tree are included in the one generated HTML file, and at the end of the file is an alphabetized index hyperlinked to the declaration of every named top-level construct.  This helps one find where globals are defined when using very large import/include trees.</para>

<para>When I am contracted for XSLT development, I deliver my end results complete with the formatted documentation.  The one HTML file includes tables, summaries, references to diagrams, and then the actual stylesheet constructs.  When using Florent's stylesheet, the Oxygen icons for constructs are presented next to each declaration (used with permission, thanks George!).</para>

<para>Feedback is welcome!  For those already using XSLStyle, this is a major release and there have been a number of changes behind the scenes, though your stylesheets do not have to (should not have to!) change.  Please let me know if you find I've introduced any problems.</para>
<para>
Input is welcome!  If anyone develops nice visual CSS presentations for XSLStyle to include in future deliveries, I would consider including them.</para>

      </answer>
    </qandaentry>

 </qandaset>
</webpage>


<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
