<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2001-06-10 10:15:15 dave"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="N5019">
<head>
<title>Looping</title>
<summary>Looping</summary>
      <keywords>xslt loops</keywords>
      <description>looping, xslt</description>

</head>
<qandaset>
<qandaentry>
    <question>
<para>Simulating a while loop</para>
</question>
    
    
      
    
    <answer>
<para role="author">David Carlisle</para>
      
      <programlisting>


&gt; Now imagine that when the conditions are such that &lt;xsl:otherwise&gt; is
&gt; matched, one wants to break out of the &lt;xsl:for-each&gt;.
    </programlisting>
    <para>It is hard to imagine that because the template instantiated
for each node selected by the xsl:for-each has no side
effects and so any test that means that you do not want to
evaluate the template on some node could have been done
before the xsl:for-each.
</para>
    <para>You only need select the nodes that you want, you don't need
to select all of the nodes and then try to `break' the loop.
</para>
    <para>It is best to think of xsl:for-each evaluating the template
on all the nodes _at the same time_. Some xslt engines may
in fact evaluate them one at a time, in document order, but
they are not obliged to do that.
</para>
    <para>You can of course implement a while loop using a recursive
named template rather than xsl:for-each.
</para>
    <para>Chris Maden adds
</para>
    <para>
Thinking words like "while" and "until" will get you into
trouble with XSLT.  Try to rephrase the question: "do this
if it's before the first foo where not bar".  For example ,
let's say you have a list of &lt;foo&gt; siblings.  One of them
has a bar attribute; you only want the &lt;foo&gt;s before the
first bar.
</para>
    <programlisting>&lt;xsl:apply-templates select="foo[following-siblings::*[@bar]]"/&gt;</programlisting>

    <para>will only select the &lt;foo&gt;s who have a following sibling
with a bar attribute.
</para>
    <para>Nikolai Grigoriev adds
</para>
    <programlisting>&gt; Imagine,
&gt; &lt;xsl:for-each select="foo"&gt;
&gt;  &lt;xsl:choose&gt;
&gt;   &lt;xsl:when test="bar"&gt;blah&lt;/xsl:when&gt;
&gt;   &lt;xsl:otherwise&gt;blort&lt;/xsl:otherwise&gt;
&gt;  &lt;/xsl:choose&gt;
&gt; &lt;/xsl:for-each&gt;
&gt; 
&gt; Now imagine that when the conditions are such that &lt;xsl:otherwise&gt; is
&gt; matched, one wants to break out of the &lt;xsl:for-each&gt;.
&gt; 
</programlisting>
    <para>How about this:
</para>
    <programlisting>&lt;!-- Identify the stopper - the first node that does not have a bar --&gt;
&lt;xsl:variable name="stop-id" select="generate-id(foo[not(bar)][1])"/&gt;

&lt;!-- Segregate nodes preceding the stopper --&gt;
&lt;xsl:for-each select="foo"&gt;
  &lt;xsl:if test="following-sibling::foo[generate-id() = $stop-id]"&gt;
    blah
  &lt;/xsl:if&gt;
&lt;/xsl:for-each&gt;
</programlisting>
    <para>This is simple but suspicious from the efficiency point of view. 
I suspect that the recursive solution is more economic.
</para>

    </answer>
  </qandaentry>
</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
