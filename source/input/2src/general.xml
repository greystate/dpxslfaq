<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-10-23 05:55:50 dpawson"   -->
<!DOCTYPE webpage  SYSTEM "../../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="general">
<head>
<title>General</title>
<summary>General questions</summary>
  <keywords> faq FAQ XSLT2 Questions</keywords>
</head>
 
<qandaset>


    
  <qandaentry>
   <question>
    <para>Whats new in XPATH 2.0</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">
>  Anyone care to try and pinpoint what is
> going to make XPath2.0 better rather than just different?
</literallayout>


<para>There are a few things that I think are really useful.</para>
<para>
General Steps:</para>
<para>
Now you don't have to use xsl:for-each to change to the relevant
document in order to use a key or ID. You can use the key() or id()
function within an path. For example:</para>

	<literallayout>  document('icons.xml')/key('icons', current()/@name)</literallayout>

<para>The same goes for other expressions that return nodes from nodes. For
example, you can get all the headings in an HTML document with:</para>

	<literallayout>  //(h1 | h2 | h3 | h4 | h5 | h6)</literallayout>

<para>rather than using something like:</para>

	<literallayout>  //*[self::h1 or self::h2 or self::h3 or
      self::h4 or self::h5 or self::h6]
</literallayout>
<para>as you have to in XPath 1.0.</para>
<para>

Range Expressions:</para>
<para>
Rather than using recursive template or the Piez Method to iterate a
certain number of times, you can now create sequences of a certain
length to iterate over. For example, insert five br elements with:</para>

	<literallayout>  &lt;xsl:for-each select="(1 to 5)">
    &lt;br />
  &lt;/xsl:for-each>
</literallayout>

<para>Node Identity Comparisons:</para>
<para>
No more generate-id() = generate-id() or count(.|...) = 1
constructions! Now you can do:</para>

	<literallayout>  $node1 == $node2</literallayout>

<para>to test whether two nodes are the same node. Of course the main reason
that we had to use these constructions in the first place was the lack
of grouping support, but nevertheless it's a good thing.</para>


<para>Precedes and Follows Comparisons:</para>
<para>
Now you can tell whether one node precedes another within a document
using a simple syntax:</para>

	<literallayout>  $node1 &lt;&lt; $node2</literallayout>

<para>I can't bring an example to mind right now, but I'm sure there have
been times when I've needed to do that (again, during grouping, I
think).</para>


<para>If Expressions:</para>

<para>Much as I object to keywords, being able to use if/then/else within a
select attribute rather than having an xsl:choose inside a variable,
or using a hideous construction involving concat(), substring() and 1
div 0 is a real godsend :)  For example:</para>

	<literallayout>  if (contains($string, $delimiter))
  then substring-before($string, $delimiter)
  otherwise $string
</literallayout>
<para>
And of course there are the functions:</para>

	<simplelist>
	  <member>  - upper-case() and lower-case()</member>
	  <member>  - match() and replace() (for all they're currently underspecified)</member>
	  <member>  - min() and max()</member>
</simplelist>

<para>Mike adds</para>
<para>
To add to the points made by Jeni, I think that the availability of
sequences-of-strings and sequences-of-numbers is going to give substantial
benefits when writing the more complex stylesheets: they are much more
flexible and efficient than using trees as the only data structuring
mechanism for working data</para>

   </answer>
  </qandaentry>


 <qandaentry>
   <question>
    <para>Comments</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">

</literallayout>
<para>XPath 2.0 allows comments *within* expressions (using the syntax {--
... --}), which would mean you could do:</para>

	<programlisting>  &lt;xsl:variable name="rows"
    select="//row[count(.|key('rows', @code)[1]) = 1]
            {-- select unique row elements by their code attribute --}" />
</programlisting>
<para>You can see why this might be necessary, given that XPath expressions
will contain for and if statements...</para>

   </answer>
  </qandaentry>
 


    <qandaentry>
      <question>
	<para>XSLT 2.0 writing style</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
	<literallayout>
> 
> Which one of these two styles should be preferred?
> 
> What are the advantages and shortcomings of each style 
> regarding readability, compactness, flexibility, efficiency 
> and maintainability?
> 
</literallayout>
<para>

I think it will take a while for this consensus to emerge.</para>
<para>
My own rule of thumb was until recently "use XPath to find nodes and to
compute atomic values, use XSLT to create new nodes". But with the
introduction of xsl:sequence, I've started avoiding really long
(20-line) path expressions, and have taken to breaking them up either by
using xsl:for-each and xsl:choose or by calls on stylesheet functions.</para>
	<literallayout>
> I understand this as personal preference or is this 
> preference based on some objective criteria?
</literallayout>
<para>It's based on instinctive judgements about the engineering quality of
the code, but it's far too early to judge whether my instincts are
right.
</para>
	<literallayout>> 
> I would appreciate your opinion on how do these two styles -- 
> long (20-line
> +) XPath expressions versus xslt-structured style --  score in 
> +readability,
> compactness, flexibility, efficiency and maintainability.
</literallayout>
<para>There has been an ongoing debate about the merits of using XML syntax
versus non-XML syntax for a while now, and I don't think it's going to
go away. It promises to be one of these perennials like "elements vs
attributes".</para>
<para>
Some people seem to take an instinctive dislike to having attributes in
an XML document whose content is 20 lines long. Part of the rationale is
that the newlines don't survive XML parsing, but the newlines are
essential to the readability of the code.</para>
<para>
I think it's going to be quite unusual to see XQuery parsers that report
more than one syntax error in a single compile run. The grammar is not
robust enough to allow easy recovery from syntax errors, though the
introduction of semicolons as separators in the latest draft helps.
Reporting multiple errors in XSLT is easy because of the 3-phase parsing
approach (XML parsing first, then XSLT, then XPath). This gives a
definite advantage when you're doing something on the DocBook scale.
</para>
 
	<literallayout>> In other words, why should we prefer the "XSLT style" to the 
> "XQuery style"?
</literallayout> 
<para>
I think the advantages of an XML-based syntax are:</para>
	<simplelist>
	  <member>(a) it's useful where the stylesheet includes large chunks of stuff to
copy into the result document
</member>
	  <member>(b) it's useful when you want to transform stylesheets or to do any kind
of reflection or introspection
</member>
	  <member>(c) it reuses all the XML machinery such as character encodings, base
URIs, entity references
</member>
	  <member>(d) it's much easier to provide user or vendor extensions to the
language in a controlled way.</member>
</simplelist>
<para>But there's no doubt that the XQuery style makes it much easier to write
short queries.</para>
<para>
Dimitre responds with</para>
<para>
Apart from purely stylistic reasons, I have found one case where the XSLT
style simply cannot be transformed completely into the non-xml style. This
happens, when we need to define and use variables, whose value is a
sequence.</para>
<para>
The variables that can be defined in an XPath expression are of the form:</para>

	<programlisting>  for $someVar in $XPathExpression return
</programlisting>
<para>and it works when we need a variable with atomic value.</para>
<para>
However, it is not possible using the above syntax to define a variable,
whose value is a sequence.</para>
<para>
Thus, if I would need to have my code in a single XPath expression, I would
have to write:</para>
	<programlisting>
&lt;xsl:sequence select=
              "f:scanIter($arg1 - 1, $arg2, $arg3)
                ,
             f:apply($arg2, f:scanIter($arg1 - 1, $arg2, $arg3)[last()])"
          />
</programlisting>
<para>because f:scanIter() returns a sequence and it is impossible to write:</para>

	<programlisting>         "for $vIterMinus in f:scanIter($arg1 - 1, $arg2, $arg3) return
               $vIterMinus, f:apply($arg2, $vIterMinus[last()])"
</programlisting>
<para>But writing the first expression above will cause</para>
	<programlisting>
    f:scanIter($arg1 - 1, $arg2, $arg3)
</programlisting>
<para>to be calculated twice.</para>

	<para>
Therefore, the more efficient way to express this is using XSLT-style:
</para>
	<programlisting>         &lt;xsl:variable name="vIterMinus"
                       select="f:scanIter($arg1 - 1, $arg2, $arg3)"/>

         &lt;xsl:sequence select=
          "$vIterMinus, f:apply($arg2, $vIterMinus[last()])"/>
</programlisting>


      </answer>
    </qandaentry>

 <qandaentry>
      <question>
	<para>Format number problem</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
	<literallayout>

> I want to use the function  format-number to to put a number 
> in a  money  format.  This works when the number is either 
> not signed or negatively signed. The XML we got from our 
> client has a "+" sign like this example:
> 
> &lt;xsl:value-of select="format-number(+00003345351.89,'$#,###.00')"/>
</literallayout>
<para>XPath 1.0 doesn't allow a leading plus sign in a number. You can get rid
of it using translate($num, '+', '').</para>

<para>XPath 1.0 predates XML Schema: its authors did a good job, but
predicting the contents of XML Schema would have been nothing short of
miraculous.</para>
<para>
XPath 2.0 fixes this.</para>

      </answer>
    </qandaentry>
<qandaentry>
      <question>
	<para>Always call a template after all others</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
<para>
In XSLT 2.0 you can do</para>
	<programlisting>
&lt;xsl:template match="*" mode="#all" priority="10">
  &lt;xsl:next-match/>
  &lt;xsl:call-template name="logRowId"/>
&lt;/xsl:template>
</programlisting>
      </answer>
    </qandaentry>
  <qandaentry>
      <question>
	<para>Change namespace with copy-of?</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
	<literallayout>

> is there a possibility to copy an element into another 
> namespace with means of &lt;xsl:copy-of> ? 
</literallayout>
<para>No. xsl:copy-of can only be used to create an exact copy. If you want to
change anything, you need to process each node individually using a
template rule ("or otherwise", as they say in maths exams).</para>


<para>
In XSLT 2.0 there is an &lt;xsl:namespace> instruction designed to fill
this gap.</para>
<para>
In XSLT 1.0 the usual circumvention is to create an RTF containing the
required namespace node, and then copy it (which requires xx:node-set):</para>
	<programlisting>
&lt;xsl:variable name="dummy">
 &lt;xsl:element name="{$prefix}:xxx" namespace="{$uri}"/>
&lt;/xsl:variable>

...

&lt;xsl:copy-of 
  select="xx:node-set($dummy)/*/namespace::*[name()=$prefix]"/>
</programlisting>
<para>Copying of namespace nodes is defined by an erratum to XSLT 1.0.</para>



      </answer>
    </qandaentry>

 <qandaentry>
      <question>
	<para>Decimal precision</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>

<para>With XSLT 2.0</para>
<para>
(a) a numeric literal such as 1.0 is interpreted as a decimal value</para>

<para>(b) you can force numbers to be treated as decimal values by casting
     e.g. xs:decimal($x)</para>

<para>(c) the result of an operation on two decimals, e.g. $x div $y, is itself
    a decimal (though with division, the precision is
implementation-defined)</para>

<para>(d) if you have a schema, and the schema defines the type of an element
    or attribute as decimal, then it's automatically treated as decimal
    without needing an explicit cast (like in (b)).</para>

<para>So summing a set of money amounts should give you the right answer without any rounding errors.</para>

      </answer>
    </qandaentry>


  <qandaentry>
      <question>
	<para>Get filename of input document</para>
      </question>
      <answer>
	<para role="author">Abel Braaksma</para>
	<literallayout>
>> Is there a way to get the file name of the document you are
>> processing? If I
>> use Document-uri() it returns the whole file path.
>
> i'm using
>
>   tokenize(document-uri(/), '/')[last()]



>
> or '\' if working with backslashes.
</literallayout>

<para>A backslash cannot be a literal part of a URI, it is not allowed. It
must be escaped for that purpose as %5C. Because a URI may well have
additional information after the 'filename', namely the query part and
the fragment part, I recommend using the regular expression that is
provided as a convenience in the RFC2396 paper. It puts the path in $5::</para>
<programlisting>
^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?
12            3  4          5       6  7        8 9</programlisting>
<para>
You can use the tokenize function above for splitting $5. Not however
that that a path separator not necessarily be a slash, it depends on the
scheme part ($2) what is and what is not allowed in the path expression.</para>




      </answer>
    </qandaentry>






    <qandaentry>
      <question>
	<para>Convert Latin characters to plain</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
	<literallayout>
> I am looking for some xslt to take a string of latin script
> unicode characters and flatten them to their plain
> equivalents. For example the E7 would become c, F1 would become
> n, F6 would become o and so on.
</literallayout>
<para>
In XPath 2.0 I think you can achieve this using</para>

<programlisting>
codepoints-to-string(string-to-codepoints(normalize-unicode($in,
='NFKD'))[. &amp;lt; 127])</programlisting>
<para>
This splits composite characters into the base character plus modifiers,
then strips off the modifiers.</para>



      </answer>
    </qandaentry>

 <qandaentry>
      <question>
	<para>Identity transform, XSLT 2.0</para>
      </question>
      <answer>
	<para role="author">Andrew Welch, Mike Kay</para>



	<programlisting>&lt;xsl:template match="element()">
  &lt;xsl:copy>
    &lt;xsl:apply-templates select="@*,node()"/>
   &lt;/xsl:copy>
&lt;/xsl:template>

&lt;xsl:template match="attribute()|text()|comment()|processing-instruction()">
  &lt;xsl:copy/>
&lt;/xsl:template></programlisting>
      </answer>
    </qandaentry>

   


    <qandaentry>
      <question>
	<para>Shortest path between two nodes</para>
      </question>
      <answer>
	<para role="author">David Carlisle</para>
	<literallayout>


>In the xml structure below, imagine:
>..- all nodes have name (not necessarily unique) and id (unique)
>attributes
>- you are in the context of node[@id='a3'] and you want to build a path
>from the name attribute to node[@id='b3'] that would result in the
>shortest path, which would be:

>../../../bar/baz/bat

>- and in another case you are in the context of node[@id='c3'] and you
>want the shortest path to node[@id='b3'], which would be:

>../../baz/bat

>- and in another case you are in the context of node[@id='b4'] and you
>want the shortest path to node[@id='b3'], which would be:

>bat

>What is the most efficient to find those paths? (this is to create page
>relative links, rather than root relative, for html pages)
</literallayout>

<para>
This doesn't get quite the result you ask for I get ../bat for the last
one, also I assume that nodes without a name should not contribute to
the path (otherwise I'd need an extra ../ to get out of the unnamed
node that contains c2).</para>

	<programlisting>%lt;xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:f="data:,f">
 
%lt;xsl:key name="id" match="*" use="@id"/>
%lt;xsl:variable name="r" select="/"/>
%lt;xsl:template match="/">

a3 to b3 %lt;xsl:value-of select="f:path('a3','b3')"/>
c3 to b3 %lt;xsl:value-of select="f:path('c3','b3')"/>
b4 to b3 %lt;xsl:value-of select="f:path('b4','b3')"/>

%lt;/xsl:template>


%lt;xsl:function name="f:path">
 %lt;xsl:param name="a"/>
 %lt;xsl:param name="b"/>
 %lt;xsl:variable name="an" select="key('id',$a,$r)"/>
 %lt;xsl:variable name="bn" select="key('id',$b,$r)"/>
 %lt;xsl:value-of>
  %lt;xsl:sequence select="($an/ancestor-or-self::node[@name] except $bn/ancestor::node)/'../'"/>
  %lt;xsl:value-of separator="/" select="($bn/ancestor-or-self::node[@name] except $an/ancestor::node)/@name"/>
 %lt;/xsl:value-of>
%lt;/xsl:function>

%lt;/xsl:stylesheet>
</programlisting>
      </answer>
    </qandaentry>


 </qandaset>
</webpage>


<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
