# dpxslfaq

The XSL FAQ restored (with permission) from Dave Pawson's now-defunct Web site.

Although i have included the docbook files (and the DTD that's used
for the intermediate XML file), and the ant file, the CI is just set up to
copy the static HTML files, not to rebuild. If you want to rebuild you'll need
the docbook chuner installed locally; edit the ant file as needed and rebuild,
then push changed files back to gitlab, either directly if you have access,
or as a pull request or merge request.